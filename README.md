# A simple API for animal interaction with Bearer authorization and stuff
## Variables

Put variables in Gitlab CI/CD:
```
DOCKERHUB_PASSWORD - docker hub password api key
DOCKERHUB_USER - docker hub user
DEPLOY_HOOK - deploy hook
NUGET_API_KEY - package api key (nuget.org > profile > api key)
QODANA_TOKEN - if you are using qodana, you can add (you should also uncomment the qodana field in gitlab-ci.yml)
```
```
PACKAGE_VERSION - version for nuget package (can be set manually, 
or use different variables to define new pipelines as new package version, 
for example: for gitlab - CI_PIPELINE_IID, for bitbucket - BITBUCKET_BUILD_NUMBER).
```
Locally (environment), as well as database connection strings:
```
DeployConnection - database connection string
```
***

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE) file for details
