﻿using AutoMapper;
using Moq;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Animal.Tests;

public static class MockSetup
{
    public static void SetupMapperMocks(Mock<IMapper> mapperMock)
    {
        var animalClassId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        var animalId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        var locationId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        var movementPointId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        mapperMock.Setup(m => m.Map<AnimalClassViewModel>(It.IsAny<AnimalClassEntity>()))
            .Returns(
                new AnimalClassViewModel 
                { 
                    Id = animalClassId,
                    Name = "Animal Class Name",
                    Description = "Animal Class Description",
                    Domain = "Animal Class Domain",
                    Realm = "Animal Class Realm",
                    Type = "Animal Class Type",
                    InternationalScientificName = "Animal Class International Scientific Name",
                    EncyclopediaUrl = "Animal Class Encyclopedia Url"
                });
        
        mapperMock.Setup(m => m.Map<AnimalViewModel>(It.IsAny<AnimalEntity>()))
            .Returns(
                new AnimalViewModel 
                { 
                    Id = animalId,
                    Name = "Animal Name",
                    Description = "Animal Description",
                    Squad = "Animal Squad",
                    Family = "Animal Family",
                    Rod = "Animal Rod",
                    View = "Animal View",
                    InternationalScientificName = "Animal International Scientific Name",
                    EncyclopediaUrl = "Animal Encyclopedia Url",
                    Population = 1,
                    PhotoUrl = "Animal Photo Url",
                    AnimalClassId = animalClassId
                });
        
        mapperMock.Setup(m => m.Map<LocationViewModel>(It.IsAny<LocationEntity>()))
            .Returns(
                new LocationViewModel 
                { 
                    Id = locationId,
                    Name = "Location Name",
                    Description = "Location Description",
                    Latitude = 1,
                    Longitude = 1
                });
        
        mapperMock.Setup(m => m.Map<MovementPointViewModel>(It.IsAny<MovementPointEntity>()))
            .Returns(
                new MovementPointViewModel 
                { 
                    Id = movementPointId,
                    AnimalId = animalId,
                    LocationId = locationId
                });
    }
    
    public static void SetupAnimalClassRepositoryMock(Mock<IAnimalClassRepository> repositoryMock)
    {
        var animalClassId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        repositoryMock.Setup(r => r.GetAnimalClassAsync(animalClassId))
            .ReturnsAsync(new AnimalClassEntity
            {
                Id = animalClassId,
                Name = "Animal Class Name",
                Description = "Animal Class Description",
                Domain = "Animal ClassDomain",
                Realm = "Animal Class Realm",
                Type = "Animal Class Type",
                InternationalScientificName = "Animal Class International Scientific Name",
                EncyclopediaUrl = "Animal Class Encyclopedia Url"
            });

        repositoryMock.Setup(r => r.CreateAnimalClassAsync(It.IsAny<AnimalClassEntity>(), It.IsAny<CancellationToken>()))
            .Returns(Task.CompletedTask);

        repositoryMock.Setup(r => r.UpdateAnimalClassAsync(It.IsAny<AnimalClassEntity>(), It.IsAny<CancellationToken>()))
            .Returns(Task.CompletedTask);

        repositoryMock.Setup(r => r.DeleteAnimalClassAsync(It.IsAny<AnimalClassEntity>(), It.IsAny<CancellationToken>()))
            .Returns(Task.CompletedTask);
    }
    
    public static void SetupAnimalRepositoryMock(Mock<IAnimalRepository> repositoryMock)
    {
        var animalId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        var animalClassId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        repositoryMock.Setup(r => r.GetAnimalAsync(animalId))
            .ReturnsAsync(new AnimalEntity
            {
                Id = animalId,
                Name = "Animal Name",
                Description = "Animal Description",
                Squad = "Animal Squad",
                Family = "Animal Family",
                Rod = "Animal Rod",
                View = "Animal View",
                InternationalScientificName = "Animal International Scientific Name",
                EncyclopediaUrl = "Animal Encyclopedia Url",
                Population = 1,
                PhotoUrl = "Animal Photo Url",
                AnimalClassId = animalClassId
            });

        repositoryMock.Setup(r => r.CreateAnimalAsync(It.IsAny<AnimalEntity>(), It.IsAny<CancellationToken>()))
            .Returns(Task.CompletedTask);

        repositoryMock.Setup(r => r.UpdateAnimalAsync(It.IsAny<AnimalEntity>(), It.IsAny<CancellationToken>()))
            .Returns(Task.CompletedTask);

        repositoryMock.Setup(r => r.DeleteAnimalAsync(It.IsAny<AnimalEntity>(), It.IsAny<CancellationToken>()))
            .Returns(Task.CompletedTask);
    }
    
    public static void SetupLocationRepositoryMock(Mock<ILocationRepository> repositoryMock)
    {
        var locationId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        repositoryMock.Setup(r => r.GetLocationAsync(locationId))
            .ReturnsAsync(new LocationEntity
            {
                Id = locationId,
                Name = "Location Name",
                Description = "Location Description",
                Latitude = 1,
                Longitude = 1
            });

        repositoryMock.Setup(r => r.CreateLocationAsync(It.IsAny<LocationEntity>(), It.IsAny<CancellationToken>()))
            .Returns(Task.CompletedTask);

        repositoryMock.Setup(r => r.UpdateLocationAsync(It.IsAny<LocationEntity>(), It.IsAny<CancellationToken>()))
            .Returns(Task.CompletedTask);

        repositoryMock.Setup(r => r.DeleteLocationAsync(It.IsAny<LocationEntity>(), It.IsAny<CancellationToken>()))
            .Returns(Task.CompletedTask);
    }
    
    public static void SetupMovementPointRepositoryMock(Mock<IMovementPointRepository> repositoryMock)
    {
        var movementPointId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        var animalId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        var locationId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        repositoryMock.Setup(r => r.GetMovementPointAsync(animalId))
            .ReturnsAsync(new MovementPointEntity
            {
                Id = movementPointId,
                AnimalId = animalId,
                LocationId = locationId
            });

        repositoryMock.Setup(r => r.CreateMovementPointAsync(It.IsAny<MovementPointEntity>(), It.IsAny<CancellationToken>()))
            .Returns(Task.CompletedTask);

        repositoryMock.Setup(r => r.UpdateMovementPointAsync(It.IsAny<MovementPointEntity>(), It.IsAny<CancellationToken>()))
            .Returns(Task.CompletedTask);

        repositoryMock.Setup(r => r.DeleteMovementPointAsync(It.IsAny<MovementPointEntity>(), It.IsAny<CancellationToken>()))
            .Returns(Task.CompletedTask);
    }
}