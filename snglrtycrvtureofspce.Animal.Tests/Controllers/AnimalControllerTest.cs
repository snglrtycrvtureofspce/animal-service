﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Moq;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.CreateAnimal;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.DeleteAnimal;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.GetAnimal;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.GetAnimalList;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.UpdateAnimal;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.File;

namespace snglrtycrvtureofspce.Animal.Tests.Controllers;

public class AnimalControllerTest
{
    private readonly Mock<IAnimalClassRepository> _animalClassRepository = new();
    
    private readonly Mock<IAnimalRepository> _animalRepository = new();
    
    private readonly Mock<ISystemApi> _systemApi = new();
    
    private readonly Mock<IMapper> _mapper = new();
    
    [SetUp]
    public Task SetupAsync()
    {
        MockSetup.SetupAnimalClassRepositoryMock(_animalClassRepository);
        MockSetup.SetupAnimalRepositoryMock(_animalRepository);
        MockSetup.SetupMapperMocks(_mapper);
        return Task.CompletedTask;
    }
    
    [Test(Author = "Aleksandr Zenevich", Description = "The controller provider possibility to create an animal item.")]
    public async Task Should_create_animal()
    {
        var animalClassId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        var handler = new CreateAnimalHandler(_animalClassRepository.Object, _animalRepository.Object, _systemApi.Object, 
            _mapper.Object);
        var response = await handler.Handle(new CreateAnimalRequest
        {
            Name = "Animal Name",
            Description = "Animal Description",
            Squad = "Animal Squad",
            Family = "Animal Family",
            Rod = "Animal Rod",
            View = "Animal View",
            InternationalScientificName = "Animal International Scientific Name",
            EncyclopediaUrl = "Animal Encyclopedia Url",
            Population = 1,
            PhotoUrl = null,
            AnimalClassId = animalClassId
        }, CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status201Created));
            Assert.That(response.Message, Is.EqualTo("Animal have been successfully created."));
        });
    }
    
    [Test(Author = "Aleksandr Zenevich", 
        Description = "The controller provider possibility to get an animal by identifier.")]
    public async Task Should_get_animal()
    {
        var animalId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        var handler = new GetAnimalHandler(_animalRepository.Object, _mapper.Object);
        var response = await handler.Handle(new GetAnimalRequest
        {
            Id = animalId
        }, CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(response.Message, Is.EqualTo("Animal have been successfully received."));
        });
    }
    
    [Test(Author = "Aleksandr Zenevich", Description = "The controller provider possibility to get an animal list.")]
    public async Task Should_get_animal_list()
    {
        var handler = new GetAnimalListHandler(_animalRepository.Object, _mapper.Object);
        var response = await handler.Handle(new GetAnimalListRequest(), CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(response.Message, Is.EqualTo("Animal list have been successfully received."));
        });
    }
    
    [Test(Author = "Aleksandr Zenevich", 
        Description = "The controller provider possibility to update an animal by identifier.")]
    public async Task Should_update_animal()
    {
        var animalClassId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        var animalId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        var handler = new UpdateAnimalHandler(_animalClassRepository.Object, _animalRepository.Object, 
            _systemApi.Object, _mapper.Object);
        var response = await handler.Handle(new UpdateAnimalRequest
        {
            Id = animalId,
            Name = "Animal Name",
            Description = "Animal Description",
            Squad = "Animal Squad",
            Family = "Animal Family",
            Rod = "Animal Rod",
            View = "Animal View",
            InternationalScientificName = "Animal International Scientific Name",
            EncyclopediaUrl = "Animal Encyclopedia Url",
            Population = 1,
            PhotoUrl = null,
            AnimalClassId = animalClassId
        }, CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(response.Message, Is.EqualTo("Animal have been successfully updated."));
        });
    }
    
    [Test(Author = "Aleksandr Zenevich", 
        Description = "The controller provider possibility to delete an animal by identifier.")]
    public async Task Should_delete_animal()
    {
        var animalId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        var handler = new DeleteAnimalHandler(_animalRepository.Object);
        var response = await handler.Handle(new DeleteAnimalRequest
        {
            Id = animalId
        }, CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(response.Message, Is.EqualTo("Animal have been successfully deleted."));
        });
    }
}