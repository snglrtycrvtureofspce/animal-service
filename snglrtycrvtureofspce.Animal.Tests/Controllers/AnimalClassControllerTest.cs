﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Moq;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.CreateAnimalClass;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.DeleteAnimalClass;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.GetAnimalClass;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.GetAnimalClassList;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.UpdateAnimalClass;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Animal.Tests.Controllers;

public class AnimalClassControllerTest
{
    private readonly Mock<IAnimalClassRepository> _animalClassRepository = new();
    
    private readonly Mock<IMapper> _mapper = new();
    
    [SetUp]
    public Task SetupAsync()
    {
        MockSetup.SetupAnimalClassRepositoryMock(_animalClassRepository);
        MockSetup.SetupMapperMocks(_mapper);
        return Task.CompletedTask;
    }
    
    [Test(Author = "Aleksandr Zenevich", 
        Description = "The controller provider possibility to create an animal class item.")]
    public async Task Should_create_animal_class()
    {
        var handler = new CreateAnimalClassHandler(_animalClassRepository.Object, _mapper.Object);
        var response = await handler.Handle(new CreateAnimalClassRequest
        {
            Name = "Animal Class Name",
            Description = "Animal Class Description",
            Domain = "Animal Class Domain",
            Realm = "Animal Class Realm",
            Type = "Animal Class Type",
            InternationalScientificName = "Animal Class International Scientific Name",
            EncyclopediaUrl = "Animal Class Encyclopedia Url"
        }, CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status201Created));
            Assert.That(response.Message, Is.EqualTo("Animal class have been successfully created."));
        });
    }
    
    [Test(Author = "Aleksandr Zenevich", 
        Description = "The controller provider possibility to get an animal class by identifier.")]
    public async Task Should_get_animal_class()
    {
        var animalClassId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        var handler = new GetAnimalClassHandler(_animalClassRepository.Object, _mapper.Object);
        var response = await handler.Handle(new GetAnimalClassRequest
        {
            Id = animalClassId
        }, CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(response.Message, Is.EqualTo("Animal class have been successfully received."));
        });
    }
    
    [Test(Author = "Aleksandr Zenevich", 
        Description = "The controller provider possibility to get an animal class list.")]
    public async Task Should_get_animal_class_list()
    {
        var handler = new GetAnimalClassListHandler(_animalClassRepository.Object, _mapper.Object);
        var response = await handler.Handle(new GetAnimalClassListRequest(), CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(response.Message, Is.EqualTo("Animal class list have been successfully received."));
        });
    }
    
    [Test(Author = "Aleksandr Zenevich", 
        Description = "The controller provider possibility to update an animal by identifier.")]
    public async Task Should_update_animal_class()
    {
        var animalClassId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        var handler = new UpdateAnimalClassHandler(_animalClassRepository.Object, _mapper.Object);
        var response = await handler.Handle(new UpdateAnimalClassRequest
        {
            Id = animalClassId,
            Name = "Animal Class Name",
            Description = "Animal Class Description",
            Domain = "Animal Class Domain",
            Realm = "Animal Class Realm",
            Type = "Animal Class Type",
            InternationalScientificName = "Animal Class International Scientific Name",
            EncyclopediaUrl = "Animal Class Encyclopedia Url"
        }, CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(response.Message, Is.EqualTo("Animal class have been successfully updated."));
        });
    }
    
    [Test(Author = "Aleksandr Zenevich", 
        Description = "The controller provider possibility to delete an animal class by identifier.")]
    public async Task Should_delete_animal_class()
    {
        var animalClassId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        var handler = new DeleteAnimalClassHandler(_animalClassRepository.Object);
        var response = await handler.Handle(new DeleteAnimalClassRequest
        {
            Id = animalClassId
        }, CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(response.Message, Is.EqualTo("Animal class have been successfully deleted."));
        });
    }
}