﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Moq;
using snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.CreateLocation;
using snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.DeleteLocation;
using snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.GetLocation;
using snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.GetLocationList;
using snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.UpdateLocation;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Animal.Tests.Controllers;

public class LocationControllerTest
{
    private readonly Mock<ILocationRepository> _locationRepository = new();
    
    private readonly Mock<IMapper> _mapper = new();
    
    [SetUp]
    public Task SetupAsync()
    {
        MockSetup.SetupLocationRepositoryMock(_locationRepository);
        MockSetup.SetupMapperMocks(_mapper);
        return Task.CompletedTask;
    }
    
    [Test(Author = "Aleksandr Zenevich", 
        Description = "The controller provider possibility to create a location item.")]
    public async Task Should_create_location()
    {
        var handler = new CreateLocationHandler(_locationRepository.Object, _mapper.Object);
        var response = await handler.Handle(new CreateLocationRequest
        {
            Name = "Location Name",
            Description = "Location Description",
            Latitude = 1,
            Longitude = 1
        }, CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status201Created));
            Assert.That(response.Message, Is.EqualTo("Location have been successfully created."));
        });
    }
    
    [Test(Author = "Aleksandr Zenevich", 
        Description = "The controller provider possibility to get a location by identifier.")]
    public async Task Should_get_location()
    {
        var locationId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        var handler = new GetLocationHandler(_locationRepository.Object, _mapper.Object);
        var response = await handler.Handle(new GetLocationRequest
        {
            Id = locationId
        }, CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(response.Message, Is.EqualTo("Location have been successfully received."));
        });
    }
    
    [Test(Author = "Aleksandr Zenevich", Description = "The controller provider possibility to get a location list.")]
    public async Task Should_get_location_list()
    {
        var handler = new GetLocationListHandler(_locationRepository.Object, _mapper.Object);
        var response = await handler.Handle(new GetLocationListRequest(), CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(response.Message, Is.EqualTo("Location list have been successfully received."));
        });
    }
    
    [Test(Author = "Aleksandr Zenevich",
        Description = "The controller provider possibility to update a location by identifier.")]
    public async Task Should_update_location()
    {
        var locationId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        var handler = new UpdateLocationHandler(_locationRepository.Object, _mapper.Object);
        var response = await handler.Handle(new UpdateLocationRequest
        {
            Id = locationId,
            Name = "Location Name",
            Description = "Location Description",
            Latitude = 1,
            Longitude = 1
        }, CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(response.Message, Is.EqualTo("Location have been successfully updated."));
        });
    }

    [Test(Author = "Aleksandr Zenevich", 
        Description = "The controller provider possibility to delete a location by identifier.")]
    public async Task Should_delete_animal()
    {
        var locationId = new Guid("2c12e095-71fb-4e0f-a3c0-05d6fd3276af");
        
        var handler = new DeleteLocationHandler(_locationRepository.Object);
        var response = await handler.Handle(new DeleteLocationRequest
        {
            Id = locationId
        }, CancellationToken.None);
        
        Assert.Multiple(() =>
        {
            Assert.That(response.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
            Assert.That(response.Message, Is.EqualTo("Location have been successfully deleted."));
        });
    }
}