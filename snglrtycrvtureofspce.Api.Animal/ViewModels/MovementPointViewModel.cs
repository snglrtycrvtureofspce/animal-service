﻿using System;

namespace snglrtycrvtureofspce.Api.Animal.ViewModels;

public class MovementPointViewModel
{
    #region IEntity
    public Guid Id { get; set; }
    
    public DateTime CreatedDate { get; set; }
    
    public DateTime ModificationDate { get; set; }
    #endregion
    
    public bool Verified { get; set; }
    
    public double Latitude { get; set; }
    
    public double Longitude { get; set; }
    
    public Guid UserId { get; set; }
    
    public Guid AnimalId { get; set; }
    
    public Guid LocationId { get; set; }
}