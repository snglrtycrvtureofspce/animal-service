﻿using System;
using snglrtycrvtureofspce.Api.Animal.Data.Entities.Enums;
using snglrtycrvtureofspce.Core.Base.Infrastructure;

namespace snglrtycrvtureofspce.Api.Animal.ViewModels;

public class MovementPointVerificationViewModel : IEntity
{
    #region IEntity
    public Guid Id { get; set; }
    
    public DateTime CreatedDate { get; set; }
    
    public DateTime ModificationDate { get; set; }
    #endregion
    
    public Guid UserId { get; set; }
    
    public Guid MovementPointId { get; set; }
    
    public StatusType StatusType { get; set; }
}