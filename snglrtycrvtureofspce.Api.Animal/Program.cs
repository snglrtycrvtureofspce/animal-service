using System;
using System.Reflection;
using System.Threading;
using AutoMapper;
using FluentValidation;
using Hangfire;
using Hangfire.PostgreSql;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Refit;
using Serilog;
using snglrtycrvtureofspce.Api.Animal.Configurations;
using snglrtycrvtureofspce.Api.Animal.Data;
using snglrtycrvtureofspce.Api.Animal.Filters;
using snglrtycrvtureofspce.Api.Animal.Jobs;
using snglrtycrvtureofspce.Api.Animal.Services.Implementations;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.File;
using snglrtycrvtureofspce.Api.User;
using snglrtycrvtureofspce.Core.Filters;
using snglrtycrvtureofspce.Core.Microservices.Core.Configurations;
using snglrtycrvtureofspce.Core.Microservices.Core.JwtAuth;
using snglrtycrvtureofspce.Core.Microservices.Core.ServerMiddleware;
using snglrtycrvtureofspce.Core.Middlewares;
var builder = WebApplication.CreateBuilder(args);

DotNetEnv.Env.Load();
var connectionString = Environment.GetEnvironmentVariable("DeployConnection");
builder.Services.AddDbContext<AnimalsDbContext>(options =>
{
    if (connectionString != null) options.UseNpgsql(connectionString);
});

builder.Services.AddServerControllers();
builder.Services.AddSnglrtycrvtureofspceAuthorization();

builder.Services.AddHttpContextAccessor();

Log.Logger = new LoggerConfiguration().MinimumLevel.Information().WriteTo.Console().CreateLogger();
builder.Logging.ClearProviders().AddSerilog();

builder.Services.AddMediatR(cfg => { cfg.RegisterServicesFromAssembly(typeof(Program).Assembly); });
builder.Services.AddValidatorsFromAssembly(typeof(Program).Assembly);

var mapperConfig = new MapperConfiguration(p => p.AddMaps(Assembly.GetExecutingAssembly()));
var mapper = mapperConfig.CreateMapper();
builder.Services.AddSingleton(mapper);
builder.Services.AddScoped<IMapperBase>(_ => mapper);

builder.Services.AddTransient(typeof (IPipelineBehavior<,>), typeof (RequestValidationBehavior<,>));

builder.Services.AddTransient(_ => new RefitSettings
{
    ContentSerializer = new NewtonsoftJsonContentSerializer(
        new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        }),
    CollectionFormat = CollectionFormat.Multi
});

builder.Services.AddScoped<IValidationService, ValidationService>();
builder.Services.AddScoped<IAnimalClassRepository, AnimalClassRepository>();
builder.Services.AddScoped<IAnimalRepository, AnimalRepository>();
builder.Services.AddScoped<ILocationRepository, LocationRepository>();
builder.Services.AddScoped<IMovementPointRepository, MovementPointRepository>();
builder.Services.AddScoped<IMovementPointVerificationRepository, MovementPointVerificationRepository>();

builder.Services.AddTransient(sc =>
    RestService.For<IUserApi>(
        new JwtHttpClient(builder.Configuration.GetMicroserviceHost("UserApi"),
            sc.GetService<IHttpContextAccessor>() 
            ?? throw new InvalidOperationException()), sc.GetService<RefitSettings>()));

builder.Services.AddTransient(sc =>
    RestService.For<ISystemApi>(
        new JwtHttpClient(builder.Configuration.GetMicroserviceHost("FileApi"),
            sc.GetService<IHttpContextAccessor>() 
            ?? throw new InvalidOperationException()), sc.GetService<RefitSettings>()));

builder.Services.ConfigureApiVersioning();
builder.Services.ConfigureSwagger(builder.Configuration);

/*if (Environment.GetEnvironmentVariable("DISABLE_HANGFIRE") == null)
{
    builder.Services.AddHangfire(configuration => configuration
        .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
        .UseSimpleAssemblyNameTypeSerializer()
        .UseRecommendedSerializerSettings()
        .UsePostgreSqlStorage(c =>
            c.UseNpgsqlConnection(Environment.GetEnvironmentVariable("HangfireConnection"))));
    
    builder.Services.AddHangfireServer(options =>
    {
        options.Queues = new[] { "animal-service" };
    });
}*/

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowAll", corsPolicyBuilder => 
        corsPolicyBuilder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
});

var app = builder.Build();

app.UseRouting();
app.UseCors("AllowAll");

/*if (Environment.GetEnvironmentVariable("DISABLE_HANGFIRE") == null)
{
    app.UseHangfireDashboard("/hangfire", new DashboardOptions
    {
        DashboardTitle = "Animal-Service Hangfire",
        IsReadOnlyFunc = static _ => true,
        Authorization = new[] { new DashboardAuthorizationFilter() }
    });
}*/

/*if (Environment.GetEnvironmentVariable("DISABLE_HANGFIRE") == null)
{
    using var scope = app.Services.CreateScope();
    var recurringJobManager = scope.ServiceProvider.GetRequiredService<IRecurringJobManager>();
    recurringJobManager.AddOrUpdate<DataCleanupJob>(
        "clean-up-old-data",
        job => job.CleanupOldDataAsync(CancellationToken.None),
        Cron.Minutely
    );
}*/

app.UseSwagger();
app.UseSwaggerUI(options =>
{
    foreach (var description in app.DescribeApiVersions())
    {
        options.SwaggerEndpoint($"{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
    }
});

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.UseMiddleware<ExceptionHandlingMiddleware>();

app.Run();