﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Api.Animal.Jobs;

public class DataCleanupJob(IMovementPointRepository movementPointRepository, ILogger<DataCleanupJob> logger)
{
    public async Task CleanupOldDataAsync(CancellationToken cancellationToken)
    {
        try
        {
            await movementPointRepository.DeleteOldMovementPointsAsync(DateTime.UtcNow.AddYears(-1), cancellationToken);
            logger.LogInformation("Cleaning of animal movement data is complete.");
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Error when clearing animal movement data.");
        }
    }
}