﻿using System.ComponentModel;

namespace snglrtycrvtureofspce.Api.Animal.Data.Entities.Enums;

public enum StatusType
{
    [Description("In reviewing")]
    InReviewing = 0,
    
    [Description("Approved")]
    Approved = 1,
    
    [Description("Declined")]
    Declined = 2
}