﻿using System;
using snglrtycrvtureofspce.Core.Base.Infrastructure;

namespace snglrtycrvtureofspce.Api.Animal.Data.Entities;

public class AnimalClassEntity : IEntity
{
    #region IEntity
    
    public Guid Id { get; set; }
    
    public DateTime CreatedDate { get; set; }
    
    public DateTime ModificationDate { get; set; }
    
    #endregion
    
    public string Name { get; set; }
    
    public string Description { get; set; }
    
    public string Domain { get; set; }
    
    public string Realm { get; set; }
    
    public string Type { get; set; }
    
    public string InternationalScientificName { get; set; }
    
    public string EncyclopediaUrl { get; set; }
}