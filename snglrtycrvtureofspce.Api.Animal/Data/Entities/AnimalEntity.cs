﻿using System;
using snglrtycrvtureofspce.Core.Base.Infrastructure;

namespace snglrtycrvtureofspce.Api.Animal.Data.Entities;

public class AnimalEntity : IEntity
{
    #region IEntity
    public Guid Id { get; set; }
    
    public DateTime CreatedDate { get; set; }
    
    public DateTime ModificationDate { get; set; }
    #endregion
    
    public string Name { get; set; }
    
    public string Description { get; set; }
    
    public string Squad { get; set; }
    
    public string Family { get; set; }
    
    public string Rod { get; set;  }
    
    public string View { get; set; }
    
    public string InternationalScientificName { get; set; }
    
    public string EncyclopediaUrl { get; set; }
    
    public int Population { get; set; }
    
    public string PhotoUrl { get; set; }
    
    public Guid AnimalClassId { get; set; }
    
    public virtual AnimalClassEntity AnimalClass { get; set; }
}