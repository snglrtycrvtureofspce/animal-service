﻿using System;
using snglrtycrvtureofspce.Core.Base.Infrastructure;

namespace snglrtycrvtureofspce.Api.Animal.Data.Entities;

public class MovementPointEntity : IEntity
{
    #region IEntity
    public Guid Id { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime ModificationDate { get; set; }
    #endregion
    
    public bool Verified { get; set; }
    
    public double Latitude { get; set; }
    
    public double Longitude { get; set; }
    
    public Guid UserId { get; set; }
    
    public Guid AnimalId { get; set; }
    
    public virtual AnimalEntity Animal { get; set; }
    
    public Guid LocationId { get; set; }
    
    public virtual LocationEntity Location { get; set; }
}