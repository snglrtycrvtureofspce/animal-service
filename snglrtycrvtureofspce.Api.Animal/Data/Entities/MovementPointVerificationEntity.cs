﻿using System;
using snglrtycrvtureofspce.Api.Animal.Data.Entities.Enums;
using snglrtycrvtureofspce.Core.Base.Infrastructure;

namespace snglrtycrvtureofspce.Api.Animal.Data.Entities;

public class MovementPointVerificationEntity : IEntity
{
    #region IEntity
    public Guid Id { get; set; }
    
    public DateTime CreatedDate { get; set; }
    
    public DateTime ModificationDate { get; set; }
    #endregion
    
    public StatusType StatusType { get; set; }
    
    public Guid UserId { get; set; }
    
    public Guid MovementPointId { get; set; }
    
    public MovementPointEntity MovementPoint { get; set; }
}