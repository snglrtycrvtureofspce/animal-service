﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Core.Base.Infrastructure;

namespace snglrtycrvtureofspce.Api.Animal.Data;

public class AnimalsDbContext : DbContext
{
    public virtual DbSet<AnimalClassEntity> AnimalClasses { get; init; }
    
    public virtual DbSet<AnimalEntity> Animals { get; init; }
    
    public virtual DbSet<LocationEntity> Locations { get; init; }
    
    public virtual DbSet<MovementPointEntity> MovementPoints { get; init; }
    
    public virtual DbSet<MovementPointVerificationEntity> MovementPointVerifications { get; init; }
    
    public AnimalsDbContext(DbContextOptions<AnimalsDbContext> opt) : base(opt) { }
    
    public AnimalsDbContext() { }
    
    public override int SaveChanges()
    {
        ChangeTracker.DetectChanges();
        
        var added = ChangeTracker
            .Entries()
            .Where(w => w.State == EntityState.Added)
            .Select(s => s.Entity)
            .ToList();
        
        foreach (var entry in added)
        {
            if (entry is not IEntity entity)
            {
                continue;
            }

            entity.CreatedDate = DateTime.UtcNow;
            entity.ModificationDate = DateTime.UtcNow;
        }
        
        var updated = ChangeTracker
            .Entries()
            .Where(w => w.State == EntityState.Modified)
            .Select(s => s.Entity)
            .ToList();
        
        foreach (var entry in updated)
        {
            if (entry is IEntity entity)
            {
                entity.ModificationDate = DateTime.UtcNow;
            }
        }
        
        return base.SaveChanges();
    }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
    {
        return Task.Run(SaveChanges, cancellationToken);
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AnimalClassEntity>(e =>
        {
            e.Property(x => x.Name)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.Description)
                .HasMaxLength(5000)
                .IsRequired();
            
            e.Property(x => x.Domain)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.Realm)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.Type)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.InternationalScientificName)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.EncyclopediaUrl)
                .HasMaxLength(255)
                .IsRequired();
        });
        
        modelBuilder.Entity<AnimalEntity>(e =>
        {
            e.Property(x => x.Name)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.Description)
                .HasMaxLength(5000)
                .IsRequired();
            
            e.Property(x => x.Squad)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.Family)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.Rod)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.View)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.InternationalScientificName)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.EncyclopediaUrl)
                .HasMaxLength(255)
                .IsRequired();
        });
        
        modelBuilder.Entity<LocationEntity>(e =>
        {
            e.Property(x => x.Name)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.Description)
                .HasMaxLength(5000)
                .IsRequired();
            
            e.Property(x => x.Latitude)
                .IsRequired();
            
            e.Property(x => x.Longitude)
                .IsRequired();
        });
        
        modelBuilder.Entity<MovementPointEntity>(e =>
        {
            e.Property(x => x.Latitude)
                .IsRequired();
            
            e.Property(x => x.Longitude)
                .IsRequired();
        });
        
        modelBuilder.Entity<MovementPointVerificationEntity>(e =>
        {
            e.Property(p => p.StatusType).HasConversion<string>();
        });
        
        base.OnModelCreating(modelBuilder);
    }
}