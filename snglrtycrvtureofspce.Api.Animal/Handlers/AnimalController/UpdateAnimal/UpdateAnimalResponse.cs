﻿using snglrtycrvtureofspce.Api.Animal.ViewModels;
using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.UpdateAnimal;

public class UpdateAnimalResponse : ItemResponse<AnimalViewModel>;