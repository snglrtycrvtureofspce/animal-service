﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Refit;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;
using snglrtycrvtureofspce.Api.File;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.UpdateAnimal;

public class UpdateAnimalHandler(IAnimalClassRepository animalClassRepository, IAnimalRepository animalRepository, 
    ISystemApi systemApi, IMapperBase mapper) : IRequestHandler<UpdateAnimalRequest, UpdateAnimalResponse>
{
    public async Task<UpdateAnimalResponse> Handle(UpdateAnimalRequest request, CancellationToken cancellationToken)
    {
        var animalClass = await animalClassRepository.GetAnimalClassAsync(request.AnimalClassId) 
                          ?? throw new ValidationException(AnimalClassErrors.NotFound(request.AnimalClassId));
        
        var animal = await animalRepository.GetAnimalAsync(request.Id) 
                     ?? throw new ValidationException(AnimalErrors.NotFound(request.Id));
        
        string photoUrl = null;
        
        if (request.PhotoUrl != null)
        {
            await using var photoStream = request.PhotoUrl.OpenReadStream();
            var photoStreamPart = new StreamPart(photoStream, request.PhotoUrl.FileName, request.PhotoUrl.ContentType);
            
            var uploadResponse = await systemApi.UploadSystemFilesAsync(photoStreamPart, 
                cancellationToken: cancellationToken);
            photoUrl = uploadResponse.Content.Item.FirstOrDefault()?.Url;
        }
        
        animal.Name = request.Name;
        animal.Description = request.Description;
        animal.Squad = request.Squad;
        animal.Family = request.Family;
        animal.Rod = request.Rod;
        animal.View = request.View;
        animal.InternationalScientificName = request.InternationalScientificName;
        animal.EncyclopediaUrl = request.EncyclopediaUrl;
        animal.Population = request.Population;
        animal.PhotoUrl = photoUrl;
        animal.AnimalClassId = animalClass.Id;
        
        await animalRepository.UpdateAnimalAsync(animal, cancellationToken);
        
        var model = mapper.Map<AnimalViewModel>(animal);
        
        var response = new UpdateAnimalResponse
        {
            Message = "Animal have been successfully updated.",
            StatusCode = StatusCodes.Status200OK,
            Item = model
        };
        
        return response;
    }
}