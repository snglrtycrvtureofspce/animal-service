﻿using System;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.UpdateAnimal;

public class UpdateAnimalRequest : IRequest<UpdateAnimalResponse>
{
    public Guid Id { get; set; }

    public string Name { get; set; }
    
    public string Description { get; set; }
    
    public string Squad { get; set; }
    
    public string Family { get; set; }
    
    public string Rod { get; set;  }
    
    public string View { get; set; }
    
    public string InternationalScientificName { get; set; }
    
    public string EncyclopediaUrl { get; set; }
    
    public int Population { get; set; }
    
    public IFormFile PhotoUrl { get; set; }
    
    public Guid AnimalClassId { get; set; }
}