﻿using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.DeleteAnimal;

public class DeleteAnimalResponse : DeleteResponse;