﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Core.Middlewares;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.DeleteAnimal;

public class DeleteAnimalHandler(IAnimalRepository repository) : IRequestHandler<DeleteAnimalRequest, 
    DeleteAnimalResponse>
{
    public async Task<DeleteAnimalResponse> Handle(DeleteAnimalRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var animal = await repository.GetAnimalAsync(request.Id) 
                         ?? throw new ValidationException(AnimalErrors.NotFound(request.Id));
            
            await repository.DeleteAnimalAsync(animal, cancellationToken);

            var response = new DeleteAnimalResponse
            {
                Message = "Animal have been successfully deleted.",
                StatusCode = StatusCodes.Status200OK,
                Id = request.Id
            };

            return response;
        }
        catch (DbUpdateException ex) 
            when (IsForeignKeyViolationExceptionMiddleware.CheckForeignKeyViolation(ex, out var referencedObject))
        {
            return new DeleteAnimalResponse
            {
                Message = $"Unable to delete animal. It is referenced by {referencedObject}.",
                StatusCode = StatusCodes.Status400BadRequest,
                Id = request.Id
            };
        }
    }
}