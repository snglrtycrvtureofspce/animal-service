﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.DeleteAnimal;

public class DeleteAnimalRequest : IRequest<DeleteAnimalResponse>
{
    public Guid Id { get; init; }
}