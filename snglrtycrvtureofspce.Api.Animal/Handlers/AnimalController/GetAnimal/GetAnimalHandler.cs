﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.GetAnimal;

public class GetAnimalHandler(IAnimalRepository repository, IMapperBase mapper) : IRequestHandler<GetAnimalRequest, 
    GetAnimalResponse>
{
    public async Task<GetAnimalResponse> Handle(GetAnimalRequest request, CancellationToken cancellationToken)
    {
        var animal = await repository.GetAnimalAsync(request.Id) 
                     ?? throw new ValidationException(AnimalErrors.NotFound(request.Id));

        var model = mapper.Map<AnimalViewModel>(animal);

        var response = new GetAnimalResponse
        {
            Message = "Animal have been successfully received.",
            StatusCode = StatusCodes.Status200OK,
            Item = model
        };

        return response;
    }
}