﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.GetAnimal;

public class GetAnimalRequest : IRequest<GetAnimalResponse>
{
    public Guid Id { get; init; }
}