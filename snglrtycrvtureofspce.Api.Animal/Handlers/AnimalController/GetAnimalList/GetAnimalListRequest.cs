﻿using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.GetAnimalList;

public class GetAnimalListRequest : IRequest<GetAnimalListResponse>;