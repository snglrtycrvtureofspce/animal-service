﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.GetAnimalList;

public class GetAnimalListHandler(IAnimalRepository repository, IMapperBase mapper) : 
    IRequestHandler<GetAnimalListRequest, GetAnimalListResponse>
{
    public async Task<GetAnimalListResponse> Handle(GetAnimalListRequest request, CancellationToken cancellationToken)
    {
        var animals = await repository.GetAnimalListAsync();

        var models = animals.Select(mapper.Map<AnimalViewModel>).ToList();
        
        var response = new GetAnimalListResponse
        {
            Message = "Animal list have been successfully received.",
            StatusCode = StatusCodes.Status200OK,
            Total = models.Count,
            Elements = models
        };

        return response;
    }
}