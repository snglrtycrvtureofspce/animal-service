﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.CreateAnimal;

public class CreateAnimalRequestValidator : AbstractValidator<CreateAnimalRequest>
{
    private readonly IValidationService _validationService;
    
    public CreateAnimalRequestValidator(IValidationService validationService)
    {
        _validationService = validationService;
        
        RuleFor(command => command.Name)
            .MaximumLength(255).WithMessage("Name has a maximum length of 255.")
            .NotNull().WithMessage("Name cannot be null.")
            .NotEmpty().WithMessage("Name cannot be empty.");
        
        RuleFor(command => command.Description)
            .MaximumLength(5000).WithMessage("Description has a maximum length of 5000.")
            .NotNull().WithMessage("Description cannot be null.")
            .NotEmpty().WithMessage("Description cannot be empty.");
        
        RuleFor(command => command.Squad)
            .MaximumLength(255).WithMessage("Squad has a maximum length of 255.")
            .NotNull().WithMessage("Squad cannot be null.")
            .NotEmpty().WithMessage("Squad cannot be empty.");
        
        RuleFor(command => command.Family)
            .MaximumLength(255).WithMessage("Family has a maximum length of 255.")
            .NotNull().WithMessage("Family cannot be null.")
            .NotEmpty().WithMessage("Family cannot be empty.");
        
        RuleFor(command => command.Rod)
            .MaximumLength(255).WithMessage("Rod has a maximum length of 255.")
            .NotNull().WithMessage("Rod cannot be null.")
            .NotEmpty().WithMessage("Rod cannot be empty.");
        
        RuleFor(command => command.View)
            .MaximumLength(255).WithMessage("View has a maximum length of 255.")
            .NotNull().WithMessage("View cannot be null.")
            .NotEmpty().WithMessage("View cannot be empty.");
        
        RuleFor(command => command.InternationalScientificName)
            .MaximumLength(255).WithMessage("International scientific name has a maximum length of 255.")
            .NotNull().WithMessage("International scientific name cannot be null.")
            .NotEmpty().WithMessage("International scientific name cannot be empty.");
        
        RuleFor(command => command.EncyclopediaUrl)
            .MaximumLength(255).WithMessage("Url of the encyclopedia has a maximum length of 255.")
            .NotNull().WithMessage("Url of the encyclopedia cannot be null.")
            .NotEmpty().WithMessage("Url of the encyclopedia cannot be empty.");
        
        RuleFor(x => x.AnimalClassId)
            .MustAsync(BeValidAnimalClass)
            .WithMessage("Invalid animal class.");
    }
    
    private async Task<bool> BeValidAnimalClass(Guid animalClassId, CancellationToken cancellationToken)
    {
        return await _validationService.IsValidAnimalClassAsync(animalClassId, cancellationToken);
    }
}