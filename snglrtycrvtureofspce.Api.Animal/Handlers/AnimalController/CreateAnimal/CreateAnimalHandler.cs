﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Refit;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;
using snglrtycrvtureofspce.Api.File;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.CreateAnimal;

public class CreateAnimalHandler(IAnimalClassRepository animalClassRepository, IAnimalRepository animalRepository, 
    ISystemApi systemApi, IMapperBase mapper) : IRequestHandler<CreateAnimalRequest, CreateAnimalResponse>
{
    public async Task<CreateAnimalResponse> Handle(CreateAnimalRequest request, CancellationToken cancellationToken)
    {
        var animalClass = await animalClassRepository.GetAnimalClassAsync(request.AnimalClassId) 
                          ?? throw new ValidationException(AnimalClassErrors.NotFound(request.AnimalClassId));
        
        string photoUrl = null;
        
        if (request.PhotoUrl != null)
        {
            await using var photoStream = request.PhotoUrl.OpenReadStream();
            var photoStreamPart = new StreamPart(photoStream, request.PhotoUrl.FileName, request.PhotoUrl.ContentType);
            
            var uploadResponse = await systemApi.UploadSystemFilesAsync(photoStreamPart, 
                cancellationToken: cancellationToken);
            photoUrl = uploadResponse.Content.Item.FirstOrDefault()?.Url;
        }
        
        var animal = new AnimalEntity
        {
            Id = Guid.NewGuid(),
            Name = request.Name,
            Description = request.Description,
            Squad = request.Squad,
            Family = request.Family,
            Rod = request.Rod,
            View = request.View,
            InternationalScientificName = request.InternationalScientificName,
            EncyclopediaUrl = request.EncyclopediaUrl,
            Population = request.Population,
            PhotoUrl = photoUrl,
            AnimalClassId = animalClass.Id
        };
        
        await animalRepository.CreateAnimalAsync(animal, cancellationToken);

        var model = mapper.Map<AnimalViewModel>(animal);

        var response = new CreateAnimalResponse
        {
            Message = "Animal have been successfully created.",
            StatusCode = StatusCodes.Status201Created,
            Item = model
        };

        return response;
    }
}