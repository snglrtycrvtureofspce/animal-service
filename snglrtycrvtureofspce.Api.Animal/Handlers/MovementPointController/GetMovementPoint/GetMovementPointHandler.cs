﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.GetMovementPoint;

public class GetMovementPointHandler(IMovementPointRepository repository, IMapperBase mapper) : 
    IRequestHandler<GetMovementPointRequest, GetMovementPointResponse>
{
    public async Task<GetMovementPointResponse> Handle(GetMovementPointRequest request, 
        CancellationToken cancellationToken)
    {
        var movementPoint = await repository.GetMovementPointAsync(request.Id) 
                            ?? throw new ValidationException(MovementPointErrors.NotFound(request.Id));
        
        var model = mapper.Map<MovementPointViewModel>(movementPoint);

        var response = new GetMovementPointResponse
        {
            Message = "Movement point have been successfully received.",
            StatusCode = StatusCodes.Status200OK,
            Item = model
        };

        return response;
    }
}