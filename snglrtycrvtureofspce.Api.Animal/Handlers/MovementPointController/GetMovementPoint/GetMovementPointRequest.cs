﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.GetMovementPoint;

public class GetMovementPointRequest : IRequest<GetMovementPointResponse>
{
    public Guid Id { get; init; }
}