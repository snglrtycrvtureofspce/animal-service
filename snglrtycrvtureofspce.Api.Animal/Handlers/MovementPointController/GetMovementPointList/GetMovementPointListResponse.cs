﻿using snglrtycrvtureofspce.Api.Animal.ViewModels;
using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.GetMovementPointList;

public class GetMovementPointListResponse : PageViewResponse<MovementPointViewModel>;