﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;
using snglrtycrvtureofspce.Api.User;
using snglrtycrvtureofspce.Core.Microservices.Core.JwtAuth.Entities.Enums;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.GetMovementPointList;

public class GetMovementPointListHandler(IMovementPointRepository repository, IMapperBase mapper, IUserApi userApi) : 
    IRequestHandler<GetMovementPointListRequest, GetMovementPointListResponse>
{
    public async Task<GetMovementPointListResponse> Handle(GetMovementPointListRequest request, 
        CancellationToken cancellationToken)
    {
        var userRole = userApi.GetUserRoleAsync(request.UserId, cancellationToken: cancellationToken);
        
        IEnumerable<MovementPointEntity> movementPoints;
        
        if (userRole.Result.Content != null && userRole.Result.Content.Item == RoleType.Administrator.ToString())
        {
            movementPoints = (await repository.GetMovementPointListAsync())
                .OrderByDescending(mp => mp.ModificationDate)
                .ToList();
        }
        else if (userRole.Result.Content != null && userRole.Result.Content.Item == RoleType.Member.ToString())
        {
            movementPoints = (await repository.GetMovementPointListAsync())
                .Where(mp => mp.Verified == true)
                .OrderByDescending(mp => mp.ModificationDate)
                .ToList();
        }
        else
        {
            return new GetMovementPointListResponse
            {
                Message = "Movement point list was not received.",
                StatusCode = StatusCodes.Status200OK
            };
        }
        
        var models = movementPoints.Select(mapper.Map<MovementPointViewModel>).ToList();
        
        var response = new GetMovementPointListResponse
        {
            Message = "Movement point list have been successfully received.",
            StatusCode = StatusCodes.Status200OK,
            Total = models.Count,
            Elements = models
        };
        
        return response;
    }
}