﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.GetMovementPointList;

public class GetMovementPointListRequest : IRequest<GetMovementPointListResponse>
{
    public Guid UserId { get; init; }
}