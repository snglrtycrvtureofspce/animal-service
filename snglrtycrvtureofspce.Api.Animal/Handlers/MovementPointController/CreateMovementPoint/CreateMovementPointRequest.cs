﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.CreateMovementPoint;

public class CreateMovementPointRequest : IRequest<CreateMovementPointResponse>
{
    public double Latitude { get; set; }
    
    public double Longitude { get; set; }
    
    public Guid UserId { get; set; }
    
    public Guid AnimalId { get; set; }
    
    public Guid LocationId { get; set; }
}