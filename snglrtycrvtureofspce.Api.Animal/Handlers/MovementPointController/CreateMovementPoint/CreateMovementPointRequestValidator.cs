﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.CreateMovementPoint;

public class CreateMovementPointRequestValidator : AbstractValidator<CreateMovementPointRequest>
{
    private readonly IValidationService _validationService;
    
    public CreateMovementPointRequestValidator(IValidationService validationService)
    {
        _validationService = validationService;
        
        RuleFor(x => x.AnimalId)
            .MustAsync(BeValidAnimal)
            .WithMessage("Invalid animal.");
        
        RuleFor(x => x.LocationId)
            .MustAsync(BeValidLocation)
            .WithMessage("Invalid location.");
    }
    
    private async Task<bool> BeValidAnimal(Guid animaId, CancellationToken cancellationToken)
    {
        return await _validationService.IsValidAnimalAsync(animaId, cancellationToken);
    }
    
    private async Task<bool> BeValidLocation(Guid locationId, CancellationToken cancellationToken)
    {
        return await _validationService.IsValidLocationAsync(locationId, cancellationToken);
    }
}