﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.CreateMovementPointVerification;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;
using snglrtycrvtureofspce.Api.User;
using snglrtycrvtureofspce.Core.Microservices.Core.JwtAuth.Entities.Enums;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.CreateMovementPoint;

public class CreateMovementPointHandler(IMovementPointRepository repository, IMapperBase mapper, IUserApi userApi, 
    ISender sender) : IRequestHandler<CreateMovementPointRequest, CreateMovementPointResponse>
{
    public async Task<CreateMovementPointResponse> Handle(CreateMovementPointRequest request, 
        CancellationToken cancellationToken)
    {
        var userRole = userApi.GetUserRoleAsync(request.UserId, cancellationToken: cancellationToken);
        
        var isAdmin = userRole.Result.Content != null && 
                      userRole.Result.Content.Item == RoleType.Administrator.ToString();
        
        var movementPoint = new MovementPointEntity
        {
            Id = Guid.NewGuid(),
            Latitude = request.Latitude,
            Longitude = request.Longitude,
            UserId = request.UserId,
            AnimalId = request.AnimalId,
            LocationId = request.LocationId,
            Verified = isAdmin
        };
        
        await repository.CreateMovementPointAsync(movementPoint, cancellationToken);
        
        if (!isAdmin)
        {
            await sender.Send(new CreateMovementPointVerificationRequest
            {
                MovementPointId = movementPoint.Id,
                UserId = request.UserId
            }, cancellationToken);
        }
        
        var model = mapper.Map<MovementPointViewModel>(movementPoint);
        
        var response = new CreateMovementPointResponse
        {
            Message = "Movement point have been successfully created.",
            StatusCode = StatusCodes.Status201Created,
            Item = model
        };
        
        return response;
    }
}