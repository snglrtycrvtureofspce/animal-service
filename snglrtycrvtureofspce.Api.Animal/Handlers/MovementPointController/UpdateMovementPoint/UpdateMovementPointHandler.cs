﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.UpdateMovementPoint;

public class UpdateMovementPointHandler(IMovementPointRepository repository, IMapperBase mapper) : 
    IRequestHandler<UpdateMovementPointRequest, UpdateMovementPointResponse>
{
    public async Task<UpdateMovementPointResponse> Handle(UpdateMovementPointRequest request, 
        CancellationToken cancellationToken)
    {
        var movementPoint = await repository.GetMovementPointAsync(request.Id) 
                            ?? throw new ValidationException(MovementPointErrors.NotFound(request.Id));
        
        movementPoint.Latitude = request.Latitude;
        movementPoint.Longitude = request.Longitude;
        movementPoint.AnimalId = request.AnimalId;
        movementPoint.LocationId = request.LocationId;
        
        await repository.UpdateMovementPointAsync(movementPoint, cancellationToken);
        
        var model = mapper.Map<MovementPointViewModel>(movementPoint);
        
        var response = new UpdateMovementPointResponse
        {
            Message = "Movement point have been successfully updated.",
            StatusCode = StatusCodes.Status200OK,
            Item = model
        };
        
        return response;
    }
}