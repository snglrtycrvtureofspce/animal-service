﻿using snglrtycrvtureofspce.Api.Animal.ViewModels;
using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.UpdateMovementPoint;

public class UpdateMovementPointResponse : ItemResponse<MovementPointViewModel>;