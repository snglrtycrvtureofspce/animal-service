﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.UpdateMovementPoint;

public class UpdateMovementPointRequest : IRequest<UpdateMovementPointResponse>
{
    public Guid Id { get; set; }
    
    public double Latitude { get; set; }
    
    public double Longitude { get; set; }

    public Guid AnimalId { get; set; }
    
    public Guid LocationId { get; set; }
}