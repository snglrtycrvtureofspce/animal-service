﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Core.Middlewares;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.DeleteMovementPoint;

public class DeleteMovementPointHandler(IMovementPointRepository repository) : 
    IRequestHandler<DeleteMovementPointRequest, DeleteMovementPointResponse>
{
    public async Task<DeleteMovementPointResponse> Handle(DeleteMovementPointRequest request, 
        CancellationToken cancellationToken)
    {
        try
        {
            var movementPoint = await repository.GetMovementPointAsync(request.Id) 
                                ?? throw new ValidationException(MovementPointErrors.NotFound(request.Id));
            
            await repository.DeleteMovementPointAsync(movementPoint, cancellationToken);

            var response = new DeleteMovementPointResponse
            {
                Message = "Movement point have been successfully deleted.",
                StatusCode = StatusCodes.Status200OK,
                Id = request.Id
            };

            return response;
        }
        catch (DbUpdateException ex) 
            when (IsForeignKeyViolationExceptionMiddleware.CheckForeignKeyViolation(ex, out var referencedObject))
        {
            return new DeleteMovementPointResponse
            {
                Message = $"Unable to delete movement point. It is referenced by {referencedObject}.",
                StatusCode = StatusCodes.Status400BadRequest,
                Id = request.Id
            };
        }
    }
}