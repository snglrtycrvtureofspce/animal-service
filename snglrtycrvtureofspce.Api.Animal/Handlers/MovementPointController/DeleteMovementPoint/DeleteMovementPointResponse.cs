﻿using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.DeleteMovementPoint;

public class DeleteMovementPointResponse : DeleteResponse;