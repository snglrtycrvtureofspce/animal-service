﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.DeleteMovementPoint;

public class DeleteMovementPointRequest : IRequest<DeleteMovementPointResponse>
{
    public Guid Id { get; init; }
}