﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.GetMovementPointVerification;

public class GetMovementPointVerificationHandler(IMovementPointVerificationRepository repository, IMapperBase mapper) 
    : IRequestHandler<GetMovementPointVerificationRequest, GetMovementPointVerificationResponse>
{
    public async Task<GetMovementPointVerificationResponse> Handle(GetMovementPointVerificationRequest request, 
        CancellationToken cancellationToken)
    {
        var movementPointVerification = await repository.GetMovementPointVerificationByMovementPointIdAsync
                                            (request.MovementPointId) 
                                        ?? throw new ValidationException(MovementPointVerificationErrors
                                            .NotFound(request.MovementPointId));
        
        var model = mapper.Map<MovementPointVerificationViewModel>(movementPointVerification);
        
        var response = new GetMovementPointVerificationResponse
        {
            Message = "Movement point verification successfully have been received.",
            StatusCode = StatusCodes.Status200OK,
            Item = model
        };

        return response;
    }
}