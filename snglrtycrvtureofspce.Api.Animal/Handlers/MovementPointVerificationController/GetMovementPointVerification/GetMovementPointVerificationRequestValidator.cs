﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.GetMovementPointVerification;

public class GetMovementPointVerificationRequestValidator : AbstractValidator<GetMovementPointVerificationRequest>
{
    private readonly IValidationService _validationService;
    
    public GetMovementPointVerificationRequestValidator(IValidationService validationService)
    {
        _validationService = validationService;
        
        RuleFor(x => x.MovementPointId)
            .MustAsync(BeValidMovementPoint)
            .WithMessage("Invalid movement point.");
    }
    
    private async Task<bool> BeValidMovementPoint(Guid movementPointId, CancellationToken cancellationToken)
    {
        return await _validationService.IsValidMovementPointAsync(movementPointId, cancellationToken);
    }
}