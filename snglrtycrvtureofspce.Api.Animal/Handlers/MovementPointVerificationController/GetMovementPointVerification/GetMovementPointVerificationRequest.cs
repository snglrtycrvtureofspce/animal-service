﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.GetMovementPointVerification;

public class GetMovementPointVerificationRequest : IRequest<GetMovementPointVerificationResponse>
{
    public Guid MovementPointId { get; init; }
}