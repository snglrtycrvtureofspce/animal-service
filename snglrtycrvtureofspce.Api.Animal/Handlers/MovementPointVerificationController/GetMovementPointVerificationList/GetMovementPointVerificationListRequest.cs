﻿using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.GetMovementPointVerificationList;

public class GetMovementPointVerificationListRequest : IRequest<GetMovementPointVerificationListResponse>;