﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Data.Entities.Enums;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.GetMovementPointVerificationList;

public class GetMovementPointVerificationListHandler(IMovementPointVerificationRepository repository, IMapperBase mapper) 
    : IRequestHandler<GetMovementPointVerificationListRequest, GetMovementPointVerificationListResponse>
{
    public async Task<GetMovementPointVerificationListResponse> Handle(GetMovementPointVerificationListRequest request, 
        CancellationToken cancellationToken)
    {
        var movementPointVerifications = await repository.GetMovementPointVerificationListAsync();

        var inReviewingVerifications = movementPointVerifications
            .Where(s => s.StatusType == StatusType.InReviewing)
            .OrderBy(m => m.MovementPointId)
            .ThenBy(m => m.CreatedDate)
            .ToList();

        var groupedMovementPointVerifications = inReviewingVerifications
            .GroupBy(m => m.MovementPointId);

        var models = groupedMovementPointVerifications.SelectMany(group =>
        {
            var sortedMovementPointVerifications = group
                .OrderBy(movementPointVerification => movementPointVerification.CreatedDate);
            
            return sortedMovementPointVerifications.Select(mapper.Map<MovementPointVerificationViewModel>);
        }).ToList();
        
        var response = new GetMovementPointVerificationListResponse
        {
            Message = "Movement point verification list have been successfully received.",
            StatusCode = StatusCodes.Status200OK,
            Elements = models,
            Total = models.Count
        };

        return response;
    }
}