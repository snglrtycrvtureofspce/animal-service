﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Core.Middlewares;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.DeleteMovementPointVerification;

public class DeleteMovementPointVerificationHandler(IMovementPointVerificationRepository repository) 
    : IRequestHandler<DeleteMovementPointVerificationRequest, DeleteMovementPointVerificationResponse>
{
    public async Task<DeleteMovementPointVerificationResponse> Handle(DeleteMovementPointVerificationRequest request, 
        CancellationToken cancellationToken)
    {
        try
        {
            var movementPoint = await repository.GetMovementPointVerificationAsync(request.Id) 
                          ?? throw new ValidationException(MovementPointVerificationErrors.NotFound(request.Id));
            
            await repository.DeleteMovementPointVerificationAsync(movementPoint, cancellationToken);
            
            var response = new DeleteMovementPointVerificationResponse
            {
                Message = "Movement point verification have been successfully deleted.",
                StatusCode = StatusCodes.Status200OK,
                Id = request.Id
            };
            
            return response;
        }
        catch (DbUpdateException ex) 
            when (IsForeignKeyViolationExceptionMiddleware.CheckForeignKeyViolation(ex, out var referencedObject))
        {
            return new DeleteMovementPointVerificationResponse
            {
                Message = $"Unable to delete movement point verification. It is referenced by {referencedObject}.",
                StatusCode = StatusCodes.Status400BadRequest,
                Id = request.Id
            };
        }
    }
}