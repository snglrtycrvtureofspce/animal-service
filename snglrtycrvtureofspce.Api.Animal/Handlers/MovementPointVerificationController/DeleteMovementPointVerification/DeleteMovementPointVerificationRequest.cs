﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.DeleteMovementPointVerification;

public class DeleteMovementPointVerificationRequest : IRequest<DeleteMovementPointVerificationResponse>
{
    public Guid Id { get; init; }
}