﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.Data.Entities.Enums;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.CreateMovementPointVerification;

public class CreateMovementPointVerificationHandler(IMovementPointRepository movementPointRepository, 
    IMovementPointVerificationRepository movementPointVerificationRepository, IMapperBase mapper) 
    : IRequestHandler<CreateMovementPointVerificationRequest, CreateMovementPointVerificationResponse>
{
    public async Task<CreateMovementPointVerificationResponse> Handle(CreateMovementPointVerificationRequest request, 
        CancellationToken cancellationToken)
    {
        var existingMovementPointVerification = await movementPointVerificationRepository
            .GetMovementPointVerificationByMovementPointIdAsync(request.MovementPointId);
        
        if (existingMovementPointVerification != null)
        {
            throw new ValidationException(MovementPointVerificationErrors.AlreadyExist(request.MovementPointId));
        }
        
        var movementPoint = await movementPointRepository.GetMovementPointAsync(request.MovementPointId) 
                            ?? throw new ValidationException(MovementPointErrors.NotFound(request.MovementPointId));
        
        var movementPointVerification = new MovementPointVerificationEntity
        {
            Id = Guid.NewGuid(),
            UserId = request.UserId,
            MovementPointId = movementPoint.Id,
            StatusType = StatusType.InReviewing
        };
        
        await movementPointVerificationRepository.CreateMovementPointVerificationAsync(movementPointVerification, 
            cancellationToken);
        
        var model = mapper.Map<MovementPointVerificationViewModel>(movementPointVerification);
        
        var response = new CreateMovementPointVerificationResponse
        {
            Message = "Movement point verification have been successfully created.",
            StatusCode = StatusCodes.Status201Created,
            Item = model
        };
        
        return response;
    }
}