﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.CreateMovementPointVerification;

public class CreateMovementPointVerificationRequest : IRequest<CreateMovementPointVerificationResponse>
{
    public Guid MovementPointId { get; init; }
    
    public Guid UserId { get; init; }
}