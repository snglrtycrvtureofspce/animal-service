﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.UpdateMovementPointVerificationStatus;

public class UpdateMovementPointVerificationStatusRequest : IRequest<UpdateMovementPointVerificationStatusResponse>
{
    public Guid MovementPointId { get; init; }
    
    public bool NewStatus { get; init; }
}