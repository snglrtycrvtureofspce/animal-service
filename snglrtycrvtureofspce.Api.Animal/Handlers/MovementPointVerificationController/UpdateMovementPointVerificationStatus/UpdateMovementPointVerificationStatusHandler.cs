﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Data.Entities.Enums;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.UpdateMovementPointVerificationStatus;

public class UpdateMovementPointVerificationStatusHandler(IMovementPointRepository movementPointRepository, 
    IMovementPointVerificationRepository movementPointVerificationRepository) 
    : IRequestHandler<UpdateMovementPointVerificationStatusRequest, UpdateMovementPointVerificationStatusResponse>
{
    public async Task<UpdateMovementPointVerificationStatusResponse> Handle(
        UpdateMovementPointVerificationStatusRequest request, CancellationToken cancellationToken)
    {
        var movementPoint = await movementPointRepository.GetMovementPointAsync(request.MovementPointId) 
                            ?? throw new ValidationException(MovementPointErrors.NotFound(request.MovementPointId));
        
        var movementPointVerification = await movementPointVerificationRepository
                                            .GetMovementPointVerificationByMovementPointIdAsync(request.MovementPointId) 
                                        ?? throw new ValidationException(MovementPointVerificationErrors
                                            .VerificationRequestIsReceived(request.MovementPointId));
        
        movementPoint.Verified = request.NewStatus;
        movementPointVerification.StatusType = request.NewStatus ? StatusType.Approved : StatusType.Declined;

        await movementPointRepository.UpdateMovementPointAsync(movementPoint, cancellationToken);
        await movementPointVerificationRepository.UpdateMovementPointVerificationAsync(movementPointVerification, 
            cancellationToken);
        
        var movementPointVerificationStatus = new UpdateMovementPointVerificationStatusModel
        {
            Verified = movementPoint.Verified,
            StatusType = movementPointVerification.StatusType
        };
        
        var response = new UpdateMovementPointVerificationStatusResponse
        {
            Message = "Movement point verification status successfully have been changed.",
            StatusCode = StatusCodes.Status200OK,
            Item = movementPointVerificationStatus
        };
        
        return response;
    }
}