﻿using snglrtycrvtureofspce.Api.Animal.Data.Entities.Enums;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.UpdateMovementPointVerificationStatus;

public class UpdateMovementPointVerificationStatusModel
{
    public bool Verified { get; set; }
    
    public StatusType StatusType { get; set; }
}