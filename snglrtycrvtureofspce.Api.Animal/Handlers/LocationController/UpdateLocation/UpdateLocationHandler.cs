﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Data;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.UpdateLocation;

public class UpdateLocationHandler(ILocationRepository repository, IMapperBase mapper) : 
    IRequestHandler<UpdateLocationRequest, UpdateLocationResponse>
{
    public async Task<UpdateLocationResponse> Handle(UpdateLocationRequest request, CancellationToken cancellationToken)
    {
        var location = await repository.GetLocationAsync(request.Id) 
                       ?? throw new ValidationException(LocationErrors.NotFound(request.Id));
        
        location.Name = request.Name;
        location.Description = request.Description;
        location.Latitude = request.Latitude;
        location.Longitude = request.Longitude;
        
        await repository.UpdateLocationAsync(location, cancellationToken);
        
        var model = mapper.Map<LocationViewModel>(location);
        
        var response = new UpdateLocationResponse
        {
            Message = "Location have been successfully updated.",
            StatusCode = StatusCodes.Status200OK,
            Item = model
        };
        
        return response;
    }
}