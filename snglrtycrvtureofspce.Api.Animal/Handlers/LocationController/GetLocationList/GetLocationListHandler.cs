﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.GetLocationList;

public class GetLocationListHandler(ILocationRepository repository, IMapperBase mapper) : 
    IRequestHandler<GetLocationListRequest, GetLocationListResponse>
{
    public async Task<GetLocationListResponse> Handle(GetLocationListRequest request, 
        CancellationToken cancellationToken)
    {
        var locations = await repository.GetLocationListAsync();

        var models = locations.Select(mapper.Map<LocationViewModel>).ToList();
        
        var response = new GetLocationListResponse
        {
            Message = "Location list have been successfully received.",
            StatusCode = StatusCodes.Status200OK,
            Total = models.Count,
            Elements = models
        };

        return response;
    }
}