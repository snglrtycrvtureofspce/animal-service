﻿using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.GetLocationList;

public class GetLocationListRequest : IRequest<GetLocationListResponse>;