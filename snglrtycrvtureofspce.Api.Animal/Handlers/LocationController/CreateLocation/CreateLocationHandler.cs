﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.CreateLocation;

public class CreateLocationHandler(ILocationRepository repository, IMapperBase mapper) : 
    IRequestHandler<CreateLocationRequest, CreateLocationResponse>
{
    public async Task<CreateLocationResponse> Handle(CreateLocationRequest request, CancellationToken cancellationToken)
    {
        var location = new LocationEntity
        {
            Id = Guid.NewGuid(),
            Name = request.Name,
            Description = request.Description,
            Latitude = request.Latitude,
            Longitude = request.Longitude
        };
        
        await repository.CreateLocationAsync(location, cancellationToken);

        var model = mapper.Map<LocationViewModel>(location);

        var response = new CreateLocationResponse
        {
            Message = "Location have been successfully created.",
            StatusCode = StatusCodes.Status201Created,
            Item = model
        };

        return response;
    }
}