﻿using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.DeleteLocation;

public class DeleteLocationResponse : DeleteResponse;