﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Core.Middlewares;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.DeleteLocation;

public class DeleteLocationHandler(ILocationRepository repository) : IRequestHandler<DeleteLocationRequest, 
    DeleteLocationResponse>
{
    public async Task<DeleteLocationResponse> Handle(DeleteLocationRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var location = await repository.GetLocationAsync(request.Id) 
                           ?? throw new ValidationException(LocationErrors.NotFound(request.Id));
            
            await repository.DeleteLocationAsync(location, cancellationToken);
            
            var response = new DeleteLocationResponse
            {
                Message = "Location have been successfully deleted.",
                StatusCode = StatusCodes.Status200OK,
                Id = request.Id
            };
            
            return response;
        }
        catch (DbUpdateException ex) when (IsForeignKeyViolationExceptionMiddleware.CheckForeignKeyViolation(ex,
                                               out var referencedObject))
        {
            return new DeleteLocationResponse
            {
                Message = $"Unable to delete location. It is referenced by {referencedObject}.",
                StatusCode = StatusCodes.Status400BadRequest,
                Id = request.Id
            };
        }
    }
}