﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.DeleteLocation;

public class DeleteLocationRequest : IRequest<DeleteLocationResponse>
{
    public Guid Id { get; set; }
}