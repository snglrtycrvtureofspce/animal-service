﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.GetLocation;

public class GetLocationRequest : IRequest<GetLocationResponse>
{
    public Guid Id { get; set; }
}