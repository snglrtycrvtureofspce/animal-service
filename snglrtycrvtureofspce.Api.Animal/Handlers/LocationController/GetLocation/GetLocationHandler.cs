﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.GetLocation;

public class GetLocationHandler(ILocationRepository repository, IMapperBase mapper) : 
    IRequestHandler<GetLocationRequest, GetLocationResponse>
{
    public async Task<GetLocationResponse> Handle(GetLocationRequest request, CancellationToken cancellationToken)
    {
        var location = await repository.GetLocationAsync(request.Id) 
                       ?? throw new ValidationException(LocationErrors.NotFound(request.Id));
        
        var model = mapper.Map<LocationViewModel>(location);

        var response = new GetLocationResponse
        {
            Message = "Location have been successfully received.",
            StatusCode = StatusCodes.Status200OK,
            Item = model
        };

        return response;
    }
}