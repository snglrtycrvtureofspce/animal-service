﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.GetAnimalClass;

public class GetAnimalClassRequest : IRequest<GetAnimalClassResponse>
{
    public Guid Id { get; init; }
}