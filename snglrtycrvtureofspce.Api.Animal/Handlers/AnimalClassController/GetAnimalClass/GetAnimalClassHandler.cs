﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.GetAnimalClass;

public class GetAnimalClassHandler(IAnimalClassRepository repository, IMapperBase mapper) : 
    IRequestHandler<GetAnimalClassRequest, GetAnimalClassResponse>
{
    public async Task<GetAnimalClassResponse> Handle(GetAnimalClassRequest request, CancellationToken cancellationToken)
    {
        var animalClass = await repository.GetAnimalClassAsync(request.Id) 
                          ?? throw new ValidationException(AnimalClassErrors.NotFound(request.Id));

        var model = mapper.Map<AnimalClassViewModel>(animalClass);

        var response = new GetAnimalClassResponse
        {
            Message = "Animal class have been successfully received.",
            StatusCode = StatusCodes.Status200OK,
            Item = model
        };

        return response;
    }
}