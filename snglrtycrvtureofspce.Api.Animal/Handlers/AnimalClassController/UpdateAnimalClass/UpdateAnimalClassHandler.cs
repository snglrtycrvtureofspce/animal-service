﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.UpdateAnimalClass;

public class UpdateAnimalClassHandler(IAnimalClassRepository repository, IMapperBase mapper) : 
    IRequestHandler<UpdateAnimalClassRequest, UpdateAnimalClassResponse>
{
    public async Task<UpdateAnimalClassResponse> Handle(UpdateAnimalClassRequest request, 
        CancellationToken cancellationToken)
    {
        var animalClass = await repository.GetAnimalClassAsync(request.Id) 
                          ?? throw new ValidationException(AnimalClassErrors.NotFound(request.Id));
        
        animalClass.Name = request.Name;
        animalClass.Description = request.Description;
        animalClass.Domain = request.Domain;
        animalClass.Realm = request.Realm;
        animalClass.Type = request.Type;
        animalClass.InternationalScientificName = request.InternationalScientificName;
        animalClass.EncyclopediaUrl = request.EncyclopediaUrl;
        
        await repository.UpdateAnimalClassAsync(animalClass, cancellationToken);
        
        var model = mapper.Map<AnimalClassViewModel>(animalClass);
        
        var response = new UpdateAnimalClassResponse
        {
            Message = "Animal class have been successfully updated.",
            StatusCode = StatusCodes.Status200OK,
            Item = model
        };
        
        return response;
    }
}