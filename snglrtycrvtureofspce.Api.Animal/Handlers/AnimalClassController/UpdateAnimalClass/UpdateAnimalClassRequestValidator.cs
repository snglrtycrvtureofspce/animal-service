﻿using FluentValidation;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.UpdateAnimalClass;

public class UpdateAnimalClassRequestValidator : AbstractValidator<UpdateAnimalClassRequest>
{
    public UpdateAnimalClassRequestValidator()
    {
        RuleFor(command => command.Name)
            .MaximumLength(255).WithMessage("Name has a maximum length of 255.")
            .NotNull().WithMessage("Name cannot be null.")
            .NotEmpty().WithMessage("Name cannot be empty.");

        RuleFor(command => command.Description)
            .MaximumLength(5000).WithMessage("Description has a maximum length of 5000.")
            .NotNull().WithMessage("Description cannot be null.")
            .NotEmpty().WithMessage("Description cannot be empty.");
        
        RuleFor(command => command.Domain)
            .MaximumLength(255).WithMessage("Domain has a maximum length of 255.")
            .NotNull().WithMessage("Domain cannot be null.")
            .NotEmpty().WithMessage("Domain cannot be empty.");
        
        RuleFor(command => command.Realm)
            .MaximumLength(255).WithMessage("Realm has a maximum length of 255.")
            .NotNull().WithMessage("Realm cannot be null.")
            .NotEmpty().WithMessage("Realm cannot be empty.");
        
        RuleFor(command => command.Type)
            .MaximumLength(255).WithMessage("Type has a maximum length of 255.")
            .NotNull().WithMessage("Type cannot be null.")
            .NotEmpty().WithMessage("Type cannot be empty.");
        
        RuleFor(command => command.InternationalScientificName)
            .MaximumLength(255).WithMessage("International scientific name has a maximum length of 255.")
            .NotNull().WithMessage("International scientific name cannot be null.")
            .NotEmpty().WithMessage("International scientific name cannot be empty.");
        
        RuleFor(command => command.EncyclopediaUrl)
            .MaximumLength(255).WithMessage("Url of the encyclopedia has a maximum length of 255.")
            .NotNull().WithMessage("Url of the encyclopedia cannot be null.")
            .NotEmpty().WithMessage("Url of the encyclopedia cannot be empty.");
    }
}