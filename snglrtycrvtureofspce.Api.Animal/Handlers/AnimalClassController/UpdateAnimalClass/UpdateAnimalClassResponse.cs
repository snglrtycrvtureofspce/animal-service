﻿using snglrtycrvtureofspce.Api.Animal.ViewModels;
using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.UpdateAnimalClass;

public class UpdateAnimalClassResponse : ItemResponse<AnimalClassViewModel>;