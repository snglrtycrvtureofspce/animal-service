﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.UpdateAnimalClass;

public class UpdateAnimalClassRequest : IRequest<UpdateAnimalClassResponse>
{
    public Guid Id { get; set; }

    public string Name { get; set; }
    
    public string Description { get; set; }
    
    public string Domain { get; set; }
    
    public string Realm { get; set; }
    
    public string Type { get; set; }
    
    public string InternationalScientificName { get; set; }
    
    public string EncyclopediaUrl { get; set; }
}