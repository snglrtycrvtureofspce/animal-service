﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.GetAnimalClassList;

public class GetAnimalClassListHandler(IAnimalClassRepository repository, IMapperBase mapper) 
    : IRequestHandler<GetAnimalClassListRequest, GetAnimalClassListResponse>
{
    public async Task<GetAnimalClassListResponse> Handle(GetAnimalClassListRequest request, 
        CancellationToken cancellationToken)
    {
        var animalClasses = await repository.GetAnimalClassListAsync();

        var models = animalClasses.Select(mapper.Map<AnimalClassViewModel>).ToList();
        
        var response = new GetAnimalClassListResponse
        {
            Message = "Animal class list have been successfully received.",
            StatusCode = StatusCodes.Status200OK,
            Total = models.Count,
            Elements = models
        };

        return response;
    }
}