﻿using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.GetAnimalClassList;

public class GetAnimalClassListRequest : IRequest<GetAnimalClassListResponse>;