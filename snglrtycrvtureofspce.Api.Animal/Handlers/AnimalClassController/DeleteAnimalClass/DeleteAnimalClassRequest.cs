﻿using System;
using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.DeleteAnimalClass;

public class DeleteAnimalClassRequest : IRequest<DeleteAnimalClassResponse>
{
    public Guid Id { get; init; }
}