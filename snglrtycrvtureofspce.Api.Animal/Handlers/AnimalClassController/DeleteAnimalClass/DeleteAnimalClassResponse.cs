﻿using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.DeleteAnimalClass;

public class DeleteAnimalClassResponse : DeleteResponse;