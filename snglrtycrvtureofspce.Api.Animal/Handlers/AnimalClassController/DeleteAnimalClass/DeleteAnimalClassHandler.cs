﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Errors;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Core.Middlewares;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.DeleteAnimalClass;

public class DeleteAnimalClassHandler(IAnimalClassRepository repository) : IRequestHandler<DeleteAnimalClassRequest, 
    DeleteAnimalClassResponse>
{
    public async Task<DeleteAnimalClassResponse> Handle(DeleteAnimalClassRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var animalClass = await repository.GetAnimalClassAsync(request.Id) 
                              ?? throw new ValidationException(AnimalClassErrors.NotFound(request.Id));
            
            await repository.DeleteAnimalClassAsync(animalClass, cancellationToken);

            var response = new DeleteAnimalClassResponse
            {
                Message = "Animal class have been successfully deleted.",
                StatusCode = StatusCodes.Status200OK,
                Id = request.Id
            };

            return response;
        }
        catch (DbUpdateException ex) 
            when (IsForeignKeyViolationExceptionMiddleware.CheckForeignKeyViolation(ex, out var referencedObject))
        {
            return new DeleteAnimalClassResponse
            {
                Message = $"Unable to delete animal class. It is referenced by {referencedObject}.",
                StatusCode = StatusCodes.Status400BadRequest,
                Id = request.Id
            };
        }
    }
}