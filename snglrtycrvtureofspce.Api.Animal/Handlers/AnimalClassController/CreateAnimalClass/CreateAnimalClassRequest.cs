﻿using MediatR;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.CreateAnimalClass;

public class CreateAnimalClassRequest : IRequest<CreateAnimalClassResponse>
{
    public string Name { get; set; }
    
    public string Description { get; set; }
    
    public string Domain { get; set; }
    
    public string Realm { get; set; }
    
    public string Type { get; set; }
    
    public string InternationalScientificName { get; set; }
    
    public string EncyclopediaUrl { get; set; }
}