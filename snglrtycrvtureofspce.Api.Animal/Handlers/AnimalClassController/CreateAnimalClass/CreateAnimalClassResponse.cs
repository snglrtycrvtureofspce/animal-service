﻿using snglrtycrvtureofspce.Api.Animal.ViewModels;
using snglrtycrvtureofspce.Core.Base.Responses;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.CreateAnimalClass;

public class CreateAnimalClassResponse : ItemResponse<AnimalClassViewModel>;