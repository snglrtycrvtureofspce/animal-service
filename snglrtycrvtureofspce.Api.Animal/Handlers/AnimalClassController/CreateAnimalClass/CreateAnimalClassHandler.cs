﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.CreateAnimalClass;

public class CreateAnimalClassHandler(IAnimalClassRepository repository, IMapperBase mapper) :
    IRequestHandler<CreateAnimalClassRequest, CreateAnimalClassResponse>
{
    public async Task<CreateAnimalClassResponse> Handle(CreateAnimalClassRequest request, 
        CancellationToken cancellationToken)
    {
        var animalClass = new AnimalClassEntity
        {
            Id = Guid.NewGuid(),
            Name = request.Name,
            Description = request.Description,
            Domain = request.Domain,
            Realm = request.Realm,
            Type = request.Type,
            InternationalScientificName = request.InternationalScientificName,
            EncyclopediaUrl = request.EncyclopediaUrl
        };
        
        await repository.CreateAnimalClassAsync(animalClass, cancellationToken);
        
        var model = mapper.Map<AnimalClassViewModel>(animalClass);
        
        var response = new CreateAnimalClassResponse
        {
            Message = "Animal class have been successfully created.",
            StatusCode = StatusCodes.Status201Created,
            Item = model
        };
        
        return response;
    }
}