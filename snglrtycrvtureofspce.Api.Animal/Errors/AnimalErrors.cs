﻿using System;
using System.Collections.Generic;
using FluentValidation.Results;

namespace snglrtycrvtureofspce.Api.Animal.Errors;

public static class AnimalErrors
{
    public static IEnumerable<ValidationFailure> NotFound(Guid id) => 
        new List<ValidationFailure> { new(nameof(id), $"Animal not found. Id: {id}") };
}