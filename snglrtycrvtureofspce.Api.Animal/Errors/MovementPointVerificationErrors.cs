﻿using System;
using System.Collections.Generic;
using FluentValidation.Results;

namespace snglrtycrvtureofspce.Api.Animal.Errors;

public static class MovementPointVerificationErrors
{
    public static IEnumerable<ValidationFailure> NotFound(Guid id) => 
        new List<ValidationFailure> { new(nameof(id), $"Movement point verification not found. Id: {id}") };
    
    public static IEnumerable<ValidationFailure> AlreadyExist(Guid id) => 
        new List<ValidationFailure> { new(nameof(id), $"Movement point verification already exists. Id: {id}") };
    
    public static IEnumerable<ValidationFailure> VerificationRequestIsReceived(Guid id) => 
        new List<ValidationFailure> { new(nameof(id), 
            $"Movement point status cannot be verified until a verification request is received. Id: {id}") };
}