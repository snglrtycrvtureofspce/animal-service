﻿using System;
using System.Collections.Generic;
using FluentValidation.Results;

namespace snglrtycrvtureofspce.Api.Animal.Errors;

public static class LocationErrors
{
    public static IEnumerable<ValidationFailure> NotFound(Guid id) => 
        new List<ValidationFailure> { new(nameof(id), $"Location not found. Id: {id}") };
}