﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

public interface IValidationService
{
    Task<bool> IsValidAnimalClassAsync(Guid animalClassId, CancellationToken cancellationToken);
    
    Task<bool> IsValidAnimalAsync(Guid animalId, CancellationToken cancellationToken);
    
    Task<bool> IsValidLocationAsync(Guid animalId, CancellationToken cancellationToken);
    
    Task<bool> IsValidMovementPointAsync(Guid movementPointId, CancellationToken cancellationToken);
}
