﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;

namespace snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

public interface IAnimalClassRepository
{
    Task CreateAnimalClassAsync(AnimalClassEntity animalClass, CancellationToken cancellationToken);
    
    Task<AnimalClassEntity> GetAnimalClassAsync(Guid id);
    
    Task<IEnumerable<AnimalClassEntity>> GetAnimalClassListAsync();
    
    Task UpdateAnimalClassAsync(AnimalClassEntity animalClass, CancellationToken cancellationToken);
    
    Task DeleteAnimalClassAsync(AnimalClassEntity animalClass, CancellationToken cancellationToken);
}