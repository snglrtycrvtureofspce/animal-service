﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;

namespace snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

public interface IMovementPointRepository
{
    Task CreateMovementPointAsync(MovementPointEntity movementPoint, CancellationToken cancellationToken);
    
    Task<MovementPointEntity> GetMovementPointAsync(Guid id);
    
    Task<IEnumerable<MovementPointEntity>> GetMovementPointListAsync();
    
    Task UpdateMovementPointAsync(MovementPointEntity movementPoint, CancellationToken cancellationToken);
    
    Task DeleteOldMovementPointsAsync(DateTime cutoffDate, CancellationToken cancellationToken);
    
    Task DeleteMovementPointAsync(MovementPointEntity movementPoint, CancellationToken cancellationToken);
}