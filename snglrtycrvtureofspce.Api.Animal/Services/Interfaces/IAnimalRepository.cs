﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;

namespace snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

public interface IAnimalRepository
{
    Task CreateAnimalAsync(AnimalEntity animal, CancellationToken cancellationToken);
    
    Task<AnimalEntity> GetAnimalAsync(Guid id);
    
    Task<IEnumerable<AnimalEntity>> GetAnimalListAsync();
    
    Task UpdateAnimalAsync(AnimalEntity animal, CancellationToken cancellationToken);
    
    Task DeleteAnimalAsync(AnimalEntity animal, CancellationToken cancellationToken);
}