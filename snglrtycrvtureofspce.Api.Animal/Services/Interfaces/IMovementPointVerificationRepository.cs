﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;

namespace snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

public interface IMovementPointVerificationRepository
{
    Task CreateMovementPointVerificationAsync(MovementPointVerificationEntity movementPointVerification, 
        CancellationToken cancellationToken);
    
    Task<MovementPointVerificationEntity> GetMovementPointVerificationAsync(Guid id);

    Task<MovementPointVerificationEntity> GetMovementPointVerificationByMovementPointIdAsync(Guid movementPointId);
    
    Task<IEnumerable<MovementPointVerificationEntity>> GetMovementPointVerificationListAsync();
    
    Task UpdateMovementPointVerificationAsync(MovementPointVerificationEntity movementPointVerification, 
        CancellationToken cancellationToken);
    
    Task DeleteMovementPointVerificationAsync(MovementPointVerificationEntity movementPointVerification, 
        CancellationToken cancellationToken);
}