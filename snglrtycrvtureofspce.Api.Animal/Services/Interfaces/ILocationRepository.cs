﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;

namespace snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

public interface ILocationRepository
{
    Task CreateLocationAsync(LocationEntity location, CancellationToken cancellationToken);
    
    Task<LocationEntity> GetLocationAsync(Guid id);
    
    Task<IEnumerable<LocationEntity>> GetLocationListAsync();
    
    Task UpdateLocationAsync(LocationEntity location, CancellationToken cancellationToken);
    
    Task DeleteLocationAsync(LocationEntity location, CancellationToken cancellationToken);
}