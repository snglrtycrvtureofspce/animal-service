﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Data;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Api.Animal.Services.Implementations;

public class ValidationService(AnimalsDbContext context) : IValidationService
{
    public async Task<bool> IsValidAnimalClassAsync(Guid animalClassId, CancellationToken cancellationToken)
    {
        return await context.AnimalClasses.AnyAsync(c => c.Id == animalClassId, cancellationToken);
    }
    
    public async Task<bool> IsValidAnimalAsync(Guid animalId, CancellationToken cancellationToken)
    {
        return await context.Animals.AnyAsync(c => c.Id == animalId, cancellationToken);
    }
    
    public async Task<bool> IsValidLocationAsync(Guid locationId, CancellationToken cancellationToken)
    {
        return await context.Locations.AnyAsync(c => c.Id == locationId, cancellationToken);
    }
    
    public async Task<bool> IsValidMovementPointAsync(Guid movementPointId, CancellationToken cancellationToken)
    {
        return await context.MovementPoints.AnyAsync(c => c.Id == movementPointId, cancellationToken);
    }
}