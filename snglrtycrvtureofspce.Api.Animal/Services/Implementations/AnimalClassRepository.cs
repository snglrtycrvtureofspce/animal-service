﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Data;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Api.Animal.Services.Implementations;

public class AnimalClassRepository(AnimalsDbContext context) : IAnimalClassRepository
{
    public async Task CreateAnimalClassAsync(AnimalClassEntity animalClass, CancellationToken cancellationToken)
    {
        await context.AnimalClasses.AddAsync(animalClass, cancellationToken);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task<AnimalClassEntity> GetAnimalClassAsync(Guid id) => await context.AnimalClasses.FindAsync(id);
    
    public async Task<IEnumerable<AnimalClassEntity>> GetAnimalClassListAsync() => 
        await context.AnimalClasses.AsNoTracking().ToListAsync();
    
    public async Task UpdateAnimalClassAsync(AnimalClassEntity animalClass, CancellationToken cancellationToken)
    {
        context.AnimalClasses.Update(animalClass);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task DeleteAnimalClassAsync(AnimalClassEntity animalClass, CancellationToken cancellationToken)
    {
        context.AnimalClasses.Remove(animalClass);
        await SaveChangesAsync(cancellationToken);
    }
    
    private async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await context.SaveChangesAsync(cancellationToken);
    }
}