﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Data;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Api.Animal.Services.Implementations;

public class MovementPointVerificationRepository(AnimalsDbContext context) : IMovementPointVerificationRepository
{
    public async Task CreateMovementPointVerificationAsync(MovementPointVerificationEntity movementPointVerification, 
        CancellationToken cancellationToken)
    {
        await context.MovementPointVerifications.AddAsync(movementPointVerification, cancellationToken);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task<MovementPointVerificationEntity> GetMovementPointVerificationAsync(Guid id) => 
        await context.MovementPointVerifications.FindAsync(id);
    
    public async Task<MovementPointVerificationEntity> GetMovementPointVerificationByMovementPointIdAsync
        (Guid movementPointId) => 
        await context.MovementPointVerifications.FirstOrDefaultAsync(i => i.MovementPointId == movementPointId);
    
    public async Task<IEnumerable<MovementPointVerificationEntity>> GetMovementPointVerificationListAsync() => 
        await context.MovementPointVerifications.AsNoTracking().ToListAsync();
    
    public async Task UpdateMovementPointVerificationAsync(MovementPointVerificationEntity movementPointVerification, 
        CancellationToken cancellationToken)
    {
        context.MovementPointVerifications.Update(movementPointVerification);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task DeleteMovementPointVerificationAsync(MovementPointVerificationEntity movementPointVerification, 
        CancellationToken cancellationToken)
    {
        context.MovementPointVerifications.Remove(movementPointVerification);
        await SaveChangesAsync(cancellationToken);
    }
    
    private async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await context.SaveChangesAsync(cancellationToken);
    }
}