﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Data;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Api.Animal.Services.Implementations;

public class LocationRepository(AnimalsDbContext context) : ILocationRepository
{
    public async Task CreateLocationAsync(LocationEntity location, CancellationToken cancellationToken)
    {
        await context.Locations.AddAsync(location, cancellationToken);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task<LocationEntity> GetLocationAsync(Guid id) => await context.Locations.FindAsync(id);
    
    public async Task<IEnumerable<LocationEntity>> GetLocationListAsync() => 
        await context.Locations.AsNoTracking().ToListAsync();
    
    public async Task UpdateLocationAsync(LocationEntity location, CancellationToken cancellationToken)
    {
        context.Locations.Update(location);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task DeleteLocationAsync(LocationEntity location, CancellationToken cancellationToken)
    {
        context.Locations.Remove(location);
        await SaveChangesAsync(cancellationToken);
    }
    
    private async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await context.SaveChangesAsync(cancellationToken);
    }
}