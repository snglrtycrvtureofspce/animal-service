﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Data;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Api.Animal.Services.Implementations;

public class AnimalRepository(AnimalsDbContext context) : IAnimalRepository
{
    public async Task CreateAnimalAsync(AnimalEntity animal, CancellationToken cancellationToken)
    {
        await context.Animals.AddAsync(animal, cancellationToken);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task<AnimalEntity> GetAnimalAsync(Guid id) => await context.Animals.FindAsync(id);
    
    public async Task<IEnumerable<AnimalEntity>> GetAnimalListAsync() => 
        await context.Animals.AsNoTracking().ToListAsync();
    
    public async Task UpdateAnimalAsync(AnimalEntity animal, CancellationToken cancellationToken)
    {
        context.Animals.Update(animal);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task DeleteAnimalAsync(AnimalEntity animal, CancellationToken cancellationToken)
    {
        context.Animals.Remove(animal);
        await SaveChangesAsync(cancellationToken);
    }
    
    private async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await context.SaveChangesAsync(cancellationToken);
    }
}