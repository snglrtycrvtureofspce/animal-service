﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Api.Animal.Data;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.Services.Interfaces;

namespace snglrtycrvtureofspce.Api.Animal.Services.Implementations;

public class MovementPointRepository(AnimalsDbContext context) : IMovementPointRepository
{
    public async Task CreateMovementPointAsync(MovementPointEntity movementPoint, CancellationToken cancellationToken)
    {
        await context.MovementPoints.AddAsync(movementPoint, cancellationToken);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task<MovementPointEntity> GetMovementPointAsync(Guid id) => await context.MovementPoints.FindAsync(id);
    
    public async Task<IEnumerable<MovementPointEntity>> GetMovementPointListAsync() => 
        await context.MovementPoints.AsNoTracking().ToListAsync();
    
    public async Task UpdateMovementPointAsync(MovementPointEntity movementPoint, CancellationToken cancellationToken)
    {
        context.MovementPoints.Update(movementPoint);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task DeleteOldMovementPointsAsync(DateTime cutoffDate, CancellationToken cancellationToken)
    {
        var oldMovementPoints = await context.MovementPoints
            .Where(movementPoint => movementPoint.ModificationDate < cutoffDate)
            .ToListAsync(cancellationToken);

        context.MovementPoints.RemoveRange(oldMovementPoints);
        await SaveChangesAsync(cancellationToken);
    }
    
    public async Task DeleteMovementPointAsync(MovementPointEntity movementPoint, CancellationToken cancellationToken)
    {
        context.MovementPoints.Remove(movementPoint);
        await SaveChangesAsync(cancellationToken);
    }
    
    private async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await context.SaveChangesAsync(cancellationToken);
    }
}