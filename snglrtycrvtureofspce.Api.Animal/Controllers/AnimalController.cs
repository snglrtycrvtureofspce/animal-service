﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.CreateAnimal;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.DeleteAnimal;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.GetAnimal;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.GetAnimalList;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalController.UpdateAnimal;
using Swashbuckle.AspNetCore.Annotations;

namespace snglrtycrvtureofspce.Api.Animal.Controllers;

[ApiController]
[Route("[controller]")]
[Microsoft.AspNetCore.Authorization.Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
[Produces("application/json")]
public class AnimalController(ISender sender) : ControllerBase
{
    /// <summary>
    /// The method provider possibility to create an animal item.
    /// </summary>
    /// <param name="request">The request object containing the details of the animal to be created.</param>
    /// <returns>A newly created animal item.</returns>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST /CreateAnimal
    ///     {
    ///        "name": "Animal Name",
    ///        "description": "Animal Description",
    ///        "squad": "Animal Squad",
    ///        "family": "Animal Family",
    ///        "rod": "Animal Rod",
    ///        "internationalScientificName": "Animal International Scientific Name",
    ///        "encyclopediaUrl": "Animal Encyclopedia Url",
    ///        "population": 1,
    ///        "animalClassId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    ///     }
    /// 
    /// </remarks>
    /// <response code="201">Returns the newly created animal item.</response>
    [HttpPost(Name = "CreateAnimal")]
    [SwaggerResponse(statusCode: StatusCodes.Status201Created, type: typeof(CreateAnimalResponse))]
    public async Task<IActionResult> CreateAnimal([FromForm] CreateAnimalRequest request) => 
        Ok(await sender.Send(request));
    
    /// <summary>
    /// The method provider possibility to get an animal by identifier.
    /// </summary>
    /// <param name="id">Identifier of the animal to be received.</param>
    /// <returns>An animal item corresponding to the provided identifier.</returns>
    /// <response code="200">Returns the animal item.</response>
    /// <response code="404">If the animal is not found.</response>
    [HttpGet("{id:guid}", Name = "GetAnimal")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetAnimalResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetAnimal(Guid id) => Ok(await sender.Send(new GetAnimalRequest { Id = id }));
    
    /// <summary>
    /// The method provider possibility to receive an animal list.
    /// </summary>
    /// <returns>A list of all available animals.</returns>
    /// <response code="200">Returns the list of animals.</response>
    [HttpGet(Name = "GetAnimalList")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetAnimalListResponse))]
    public async Task<IActionResult> GetAnimalList() => Ok(await sender.Send(new GetAnimalListRequest()));
    
    /// <summary>
    /// The method provider possibility to update an animal by identifier.
    /// </summary>
    /// <param name="id">Identifier of the animal to be received.</param>
    /// <param name="request">The request object containing the details of the animal to be updated.</param>
    /// <returns>The updated animal item.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /UpdateAnimal/{id}
    ///     {
    ///        "name": "Animal Name",
    ///        "description": "Animal Description",
    ///        "squad": "Animal Squad",
    ///        "family": "Animal Family",
    ///        "rod": "Animal Rod",
    ///        "internationalScientificName": "Animal International Scientific Name",
    ///        "encyclopediaUrl": "Animal Encyclopedia Url",
    ///        "population": 1,
    ///        "photoUrl": "Animal Photo Url",
    ///        "animalClassId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    ///     }
    ///
    /// </remarks>
    /// <response code="200">Returns the updated animal item.</response>
    /// <response code="404">If the animal is not found.</response>
    [HttpPut("{id:guid}", Name = "UpdateAnimal")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(UpdateAnimalResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> UpdateAnimal(Guid id, [FromForm] UpdateAnimalRequest request)
    {
        request.Id = id;
        return Ok(await sender.Send(request));
    }

    /// <summary>
    /// The method provider possibility to delete an animal by identifier.
    /// </summary>
    /// <param name="id">Identifier of the animal to be received.</param>
    /// <returns>A confirmation of the deletion.</returns>
    /// <response code="200">Returns the confirmation of the deleted animal.</response>
    /// <response code="404">If the animal is not found.</response>
    [HttpDelete("{id:guid}", Name = "DeleteAnimal")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(DeleteAnimalResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> DeleteAnimal(Guid id) => 
        Ok(await sender.Send(new DeleteAnimalRequest { Id = id }));
}