﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.CreateMovementPointVerification;
using snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.DeleteMovementPointVerification;
using snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.GetMovementPointVerification;
using snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.GetMovementPointVerificationList;
using snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointVerificationController.UpdateMovementPointVerificationStatus;
using snglrtycrvtureofspce.Core.Microservices.Core.ServerMiddleware;
using Swashbuckle.AspNetCore.Annotations;

namespace snglrtycrvtureofspce.Api.Animal.Controllers;

[ApiController]
[Route("[controller]")]
[Microsoft.AspNetCore.Authorization.Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
[Produces("application/json")]
public class MovementPointVerificationController(ISender sender) : ControllerBase
{
    /// <summary>
    /// The method provider possibility to create a movement point verification item by movement point id and user id.
    /// </summary>
    /// <param name="movementPointId">Identifier of the movement point to be received.</param>
    /// <returns>A newly created movement point verification item.</returns>
    /// <response code="201">Returns the newly created movement point verification item.</response>
    [HttpPost("{movementPointId:guid}", Name = "CreateMovementPointVerification")]
    [SwaggerResponse(statusCode: StatusCodes.Status201Created, type: typeof(CreateMovementPointVerificationResponse))]
    public async Task<IActionResult> CreateMovementPointVerification(Guid movementPointId) => 
        Ok(await sender.Send(new CreateMovementPointVerificationRequest
        {
            MovementPointId = movementPointId,
            UserId = User.Claims.GetUserId()
        }));
    
    /// <summary>
    /// The method provider possibility to get movement point verification by movement point id.
    /// </summary>
    /// <param name="movementPointId">Identifier of the movement point to be received.</param>
    /// <returns>A movement point verification item corresponding to the provided identifier.</returns>
    /// <response code="200">Returns the movement point verification item.</response>
    /// <response code="404">If the movement point verification is not found.</response>
    [HttpGet("{movementPointId:guid}", Name = "GetMovementPointVerification")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetMovementPointVerificationResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetMovementPointVerification(Guid movementPointId) => 
        Ok(await sender.Send(new GetMovementPointVerificationRequest { MovementPointId = movementPointId }));
    
    /// <summary>
    /// The method provider possibility to get a movement point verification list that is under review.
    /// </summary>
    /// <returns>A list of all available movement point verifications.</returns>
    /// <response code="200">Returns the list of movement point verifications.</response>
    [HttpGet(Name = "GetMovementPointVerificationList")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK,type: typeof(GetMovementPointVerificationListResponse))]
    public async Task<IActionResult> GetMovementPointVerificationList() => 
        Ok(await sender.Send(new GetMovementPointVerificationListRequest()));
    
    /// <summary>
    /// The method provider possibility to update a movement point verification status by movement point id.
    /// </summary>
    /// <param name="movementPointId">Identifier of the movement point to be received.</param>
    /// <param name="request">The request object containing the details of the new status to be updated.</param>
    /// <returns>The updated movement point verification status.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /UpdateMovementPointVerificationStatus
    ///     {
    ///        "newStatus": true
    ///     }
    ///
    /// </remarks>
    /// <response code="200">Returns the updated movement point verification status.</response>
    /// <response code="404">If the movement point verification is not found.</response>
    [HttpPut("{movementPointId:guid}", Name = "UpdateMovementPointVerificationStatus")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(UpdateMovementPointVerificationStatusResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> UpdateMovementPointVerificationStatus(Guid movementPointId, 
        [FromBody] UpdateMovementPointVerificationStatusRequest request) => 
        Ok(await sender.Send(new UpdateMovementPointVerificationStatusRequest
        {
            MovementPointId = movementPointId, 
            NewStatus = request.NewStatus
        }));
    
    /// <summary>
    /// The method provider possibility to delete a movement point verification by identifier.
    /// </summary>
    /// <param name="id">The unique identifier of the movement point verification to be deleted.</param>
    /// <returns>A confirmation of the deletion.</returns>
    /// <response code="200">Returns the confirmation of the deleted movement point verification.</response>
    /// <response code="404">If the movement point verification is not found.</response>
    [HttpDelete("{id:guid}", Name = "DeleteMovementPointVerification")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(DeleteMovementPointVerificationResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> DeleteMovementPointVerification(Guid id) => 
        Ok(await sender.Send(new DeleteMovementPointVerificationRequest { Id = id }));
}