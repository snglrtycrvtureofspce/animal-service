﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.CreateAnimalClass;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.DeleteAnimalClass;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.GetAnimalClass;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.GetAnimalClassList;
using snglrtycrvtureofspce.Api.Animal.Handlers.AnimalClassController.UpdateAnimalClass;
using Swashbuckle.AspNetCore.Annotations;

namespace snglrtycrvtureofspce.Api.Animal.Controllers;

[ApiController]
[Route("[controller]")]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
[Produces("application/json")]
public class AnimalClassController(ISender sender) : ControllerBase
{
    /// <summary>
    /// The method provider possibility to create an animal class item.
    /// </summary>
    /// <param name="request">The request object containing the details of the animal class to be created.</param>
    /// <returns>A newly created animal class item.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /CreateAnimalClass
    ///     {
    ///        "name": "Animal Class Name",
    ///        "description": "Animal Class Description",
    ///        "domain": "Animal Class Domain",
    ///        "realm": "Animal Class Realm",
    ///        "type": "Animal Class Type",
    ///        "internationalScientificName": "Animal Class International Scientific Name",
    ///        "encyclopediaUrl": "Animal Class Encyclopedia Url"
    ///     }
    ///
    /// </remarks>
    /// <response code="201">Returns the newly created animal class item.</response>
    [HttpPost(Name = "CreateAnimalClass")]
    [SwaggerResponse(statusCode: StatusCodes.Status201Created, type: typeof(CreateAnimalClassResponse))]
    public async Task<IActionResult> CreateAnimalClass([FromBody] CreateAnimalClassRequest request) => 
        Ok(await sender.Send(request));
    
    /// <summary>
    /// The method provider possibility to get an animal class by identifier.
    /// </summary>
    /// <param name="id">The unique identifier of the animal class.</param>
    /// <returns>An animal class item corresponding to the provided identifier.</returns>
    /// <response code="200">Returns the animal class item.</response>
    /// <response code="404">If the animal class is not found.</response>
    [HttpGet("{id:guid}", Name = "GetAnimalClass")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetAnimalClassResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetAnimalClass(Guid id) => 
        Ok(await sender.Send(new GetAnimalClassRequest { Id = id }));
    
    /// <summary>
    /// The method provider possibility to get an animal class list.
    /// </summary>
    /// <returns>A list of all available animal classes.</returns>
    /// <response code="200">Returns the list of animal classes.</response>
    [HttpGet(Name = "GetAnimalClassList")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetAnimalClassListResponse))]
    public async Task<IActionResult> GetAnimalClassList() => Ok(await sender.Send(new GetAnimalClassListRequest()));

    /// <summary>
    /// The method provider possibility to update an animal class by identifier.
    /// </summary>
    /// <param name="id">Identifier of the animal class to be received.</param>
    /// <param name="request">The request object containing the details of the animal class to be updated.</param>
    /// <returns>The updated animal class item.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /UpdateAnimalClass/{id}
    ///     {
    ///        "name": "Animal Class Name",
    ///        "description": "Animal Class Description",
    ///        "domain": "Animal Class Domain",
    ///        "realm": "Animal Class Realm",
    ///        "type": "Animal Class Type",
    ///        "internationalScientificName": "Animal Class International Scientific Name",
    ///        "encyclopediaUrl": "Animal Class Encyclopedia Url"
    ///     }
    ///
    /// </remarks>
    /// <response code="200">Returns the updated animal class item.</response>
    /// <response code="404">If the animal class is not found.</response>
    [HttpPut("{id:guid}", Name = "UpdateAnimalClass")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(UpdateAnimalClassResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> UpdateAnimalClass(Guid id, [FromBody] UpdateAnimalClassRequest request)
    {
        request.Id = id;
        return Ok(await sender.Send(request));
    }

    /// <summary>
    /// The method provider possibility to delete an animal class by identifier.
    /// </summary>
    /// <param name="id">Identifier of the animal class to be received.</param>
    /// <returns>A confirmation of the deletion.</returns>
    /// <response code="200">Returns the confirmation of the deleted animal class.</response>
    /// <response code="404">If the animal class is not found.</response>
    [HttpDelete("{id:guid}", Name = "DeleteAnimalClass")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(DeleteAnimalClassResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> DeleteAnimalClass(Guid id) => 
        Ok(await sender.Send(new DeleteAnimalClassRequest { Id = id }));
}