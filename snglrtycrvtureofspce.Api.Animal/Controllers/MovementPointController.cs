﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.CreateMovementPoint;
using snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.DeleteMovementPoint;
using snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.GetMovementPoint;
using snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.GetMovementPointList;
using snglrtycrvtureofspce.Api.Animal.Handlers.MovementPointController.UpdateMovementPoint;
using snglrtycrvtureofspce.Core.Microservices.Core.ServerMiddleware;
using Swashbuckle.AspNetCore.Annotations;

namespace snglrtycrvtureofspce.Api.Animal.Controllers;

[ApiController]
[Route("[controller]")]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
[Produces("application/json")]
public class MovementPointController(ISender sender) : ControllerBase
{
    /// <summary>
    /// The method provider possibility to create a movement point item.
    /// </summary>
    /// <param name="request">The request object containing the details of the movement point to be created.</param>
    /// <returns>A newly created movement point item.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /CreateMovementPoint
    ///     {
    ///        "latitude": 1,
    ///        "longitude": 1
    ///        "animalId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    ///        "locationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    ///     }
    ///
    /// </remarks>
    /// <response code="201">Returns the newly created movement point item.</response>
    [HttpPost(Name = "CreateMovementPoint")]
    [SwaggerResponse(statusCode: StatusCodes.Status201Created, type: typeof(CreateMovementPointResponse))]
    public async Task<IActionResult> CreateMovementPoint([FromBody] CreateMovementPointRequest request)
    { 
        request.UserId = User.Claims.GetUserId();
        return Ok(await sender.Send(request));
    }
    
    /// <summary>
    /// The method provider possibility to get a movement point by identifier.
    /// </summary>
    /// <param name="id">Identifier of the movement point to be received.</param>
    /// <returns>A movement point item corresponding to the provided identifier.</returns>
    /// <response code="200">Returns the movement point item.</response>
    /// <response code="404">If the movement point is not found.</response>
    [HttpGet("{id:guid}", Name = "GetMovementPoint")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetMovementPointResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetMovementPoint(Guid id) => 
        Ok(await sender.Send(new GetMovementPointRequest { Id = id }));
    
    /// <summary>
    /// The method provider possibility to receive a movement point list.
    /// </summary>
    /// <returns>A list of all available movement points.</returns>
    /// <response code="200">Returns the list of movement points.</response>
    [HttpGet(Name = "GetMovementPointList")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetMovementPointListResponse))]
    public async Task<IActionResult> GetMovementPointList() => 
        Ok(await sender.Send(new GetMovementPointListRequest { UserId = User.Claims.GetUserId() }));

    /// <summary>
    /// The method provider possibility to update a movement point by identifier.
    /// </summary>
    /// <param name="id">Identifier of the movement point to be received.</param>
    /// <param name="request">The request object containing the details of the movement point to be updated.</param>
    /// <returns>The updated movement point item.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /UpdateMovementPoint/{id}
    ///     {
    ///        "latitude": 1,
    ///        "longitude": 1
    ///        "animalId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    ///        "locationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    ///     }
    ///
    /// </remarks>
    /// <response code="200">Returns the updated movement point item.</response>
    /// <response code="404">If the movement point is not found.</response>
    [HttpPut("{id:guid}", Name = "UpdateMovementPoint")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(UpdateMovementPointResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> UpdateMovementPoint(Guid id, [FromBody] UpdateMovementPointRequest request)
    {
        request.Id = id;
        return Ok(await sender.Send(request));
    }

    /// <summary>
    /// The method provider possibility to delete a movement point by identifier.
    /// </summary>
    /// <param name="id">Identifier of the movement point to be received.</param>
    /// <returns>A confirmation of the deletion.</returns>
    /// <response code="200">Returns the confirmation of the deleted movement point.</response>
    /// <response code="404">If the movement point is not found.</response>
    [HttpDelete("{id:guid}", Name = "DeleteMovementPoint")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(DeleteMovementPointResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> DeleteMovementPoint(Guid id) => 
        Ok(await sender.Send(new DeleteMovementPointRequest { Id = id }));
}