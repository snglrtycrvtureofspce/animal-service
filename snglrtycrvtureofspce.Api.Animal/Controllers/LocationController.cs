﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.CreateLocation;
using snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.DeleteLocation;
using snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.GetLocation;
using snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.GetLocationList;
using snglrtycrvtureofspce.Api.Animal.Handlers.LocationController.UpdateLocation;
using Swashbuckle.AspNetCore.Annotations;

namespace snglrtycrvtureofspce.Api.Animal.Controllers;

[ApiController]
[Route("[controller]")]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
[Produces("application/json")]
public class LocationController(ISender sender) : ControllerBase
{
    /// <summary>
    /// The method provider possibility to create a location item.
    /// </summary>
    /// <param name="request">The request object containing the details of the location to be created.</param>
    /// <returns>A newly created location item.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /CreateLocation
    ///     {
    ///        "name": "Location Name",
    ///        "description": "Location Description",
    ///        "latitude": 1,
    ///        "longitude": 1
    ///     }
    ///
    /// </remarks>
    /// <response code="201">Returns the newly created location item.</response>
    [HttpPost(Name = "CreateLocation")]
    [SwaggerResponse(statusCode: StatusCodes.Status201Created, type: typeof(CreateLocationResponse))]
    public async Task<IActionResult> CreateLocation([FromBody] CreateLocationRequest request) => 
        Ok(await sender.Send(request));
    
    /// <summary>
    /// The method provider possibility to get a location by identifier.
    /// </summary>
    /// <param name="id">Identifier of the location to be received.</param>
    /// <returns>A location item corresponding to the provided identifier.</returns>
    /// <response code="200">Returns the location item.</response>
    /// <response code="404">If the location is not found.</response>
    [HttpGet("{id:guid}", Name = "GetLocation")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetLocationResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetLocation(Guid id) => Ok(await sender.Send(new GetLocationRequest { Id = id }));
    
    /// <summary>
    /// The method provider possibility to receive a location list.
    /// </summary>
    /// <returns>A list of all available locations.</returns>
    /// <response code="200">Returns the list of locations.</response>
    [HttpGet(Name = "GetLocationList")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(GetLocationListResponse))]
    public async Task<IActionResult> GetLocationList() => Ok(await sender.Send(new GetLocationListRequest()));
    
    /// <summary>
    /// The method provider possibility to update a location by identifier.
    /// </summary>
    /// <param name="id">Identifier of the location to be received.</param>
    /// <param name="request">The request object containing the details of the location to be updated.</param>
    /// <returns>The updated location item.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /UpdateLocation/{id}
    ///     {
    ///        "name": "Location Name",
    ///        "description": "Location Description",
    ///        "latitude": 1,
    ///        "longitude": 1
    ///     }
    ///
    /// </remarks>
    /// <response code="200">Returns the updated location item.</response>
    /// <response code="404">If the location is not found.</response>
    [HttpPut("{id:guid}", Name = "UpdateLocation")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(UpdateLocationResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> UpdateLocation(Guid id, [FromBody] UpdateLocationRequest request)
    {
        request.Id = id;
        return Ok(await sender.Send(request));
    }

    /// <summary>
    /// The method provider possibility to delete a location by identifier.
    /// </summary>
    /// <param name="id">Identifier of the location to be received.</param>
    /// <returns>A confirmation of the deletion.</returns>
    /// <response code="200">Returns the confirmation of the deleted location.</response>
    /// <response code="404">If the location is not found.</response>
    [HttpDelete("{id:guid}", Name = "DeleteLocation")]
    [SwaggerResponse(statusCode: StatusCodes.Status200OK, type: typeof(DeleteLocationResponse))]
    [SwaggerResponse(statusCode: StatusCodes.Status404NotFound)]
    public async Task<IActionResult> DeleteLocation(Guid id) => 
        Ok(await sender.Send(new DeleteLocationRequest { Id = id }));
}