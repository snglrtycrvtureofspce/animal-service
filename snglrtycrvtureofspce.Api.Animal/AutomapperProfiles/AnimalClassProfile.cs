﻿using AutoMapper;
using snglrtycrvtureofspce.Api.Animal.Data.Entities;
using snglrtycrvtureofspce.Api.Animal.ViewModels;

namespace snglrtycrvtureofspce.Api.Animal.AutomapperProfiles;

public class AnimalClassProfile : Profile
{
    public AnimalClassProfile()
    {
        CreateMap<AnimalClassEntity, AnimalClassViewModel>();
    }
}